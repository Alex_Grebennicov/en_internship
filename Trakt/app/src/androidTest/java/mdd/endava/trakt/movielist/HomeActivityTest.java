package mdd.endava.trakt.movielist;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import mdd.endava.trakt.R;
import mdd.endava.trakt.home.HomeActivity;
import mdd.endava.trakt.home.view.HomeFragment;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class HomeActivityTest {

    @Rule
    public ActivityTestRule<HomeActivity> activityTestRule = new ActivityTestRule<>(HomeActivity.class);

    private HomeActivity activity = null;

    @Before
    public void setUp() throws Exception {
        activity = activityTestRule.getActivity();
    }

    @Test
    public void testActivityLaunch() {
        verifySpinnerSet();
        verifyBottomBarSet();
        verifyFragmentSet();
    }

    @Test
    public void testBottomBar() {
        verifyBottomBarSet();

        popularMenuItemClick();
        verifyFragmentSet();

        topRatedMenuItemClick();
        verifyFragmentSet();
    }

    @Test
    public void testActionBarSpinner() {
        verifySpinnerSet();
        List<String> entries = Arrays.asList(activity.getResources().getStringArray(R.array.spinner_options));
        for (String entry : entries) {
            spinnerItemAtPositionClick(entry);
            verifyFragmentSet();
        }
        for (int i = entries.size() - 1; i >= 0; i--) {
            spinnerItemAtPositionClick(entries.get(i));
            verifyFragmentSet();
        }
    }

    private void verifyFragmentSet() {
        Assert.assertNotNull(activity.getSupportFragmentManager());
        Assert.assertNotNull(activity.getSupportFragmentManager().getFragments());
        Assert.assertEquals(1, activity.getSupportFragmentManager().getFragments().size());
        Assert.assertEquals(activity.getSupportFragmentManager().getFragments().get(0).getClass().getSimpleName(), HomeFragment.class.getSimpleName());
    }

    private void verifySpinnerSet() {
        Assert.assertNotNull(activity.findViewById(R.id.spinner_actionbar));
    }

    private void verifyBottomBarSet() {
        Assert.assertNotNull(activity.findViewById(R.id.bottom_bar));
    }

    private void popularMenuItemClick() {
        Espresso.onView(ViewMatchers.withId(R.id.popular)).perform(ViewActions.click());
    }

    private void topRatedMenuItemClick() {
        Espresso.onView(ViewMatchers.withId(R.id.top_rated)).perform(ViewActions.click());
    }

    private void spinnerItemAtPositionClick(String entry) {
        Espresso.onView(ViewMatchers.withId(R.id.spinner_actionbar)).perform(ViewActions.click());
        Espresso.onData(allOf(is(instanceOf(String.class)), is(entry))).perform(ViewActions.click());
    }
}