package mdd.endava.trakt;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import mdd.endava.trakt.dagger.AppComponent;
import mdd.endava.trakt.dagger.DaggerAppComponent;
import mdd.endava.trakt.dagger.modules.ContextModule;

public class App extends Application {
    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder().schemaVersion(20).deleteRealmIfMigrationNeeded().build());
        component = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();
    }

    public static AppComponent getComponent() {
        return component;
    }
}
