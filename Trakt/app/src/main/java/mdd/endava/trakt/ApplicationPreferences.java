package mdd.endava.trakt;

import android.content.Context;
import android.content.SharedPreferences;

import mdd.endava.trakt.common.enums.ContentTag;
import mdd.endava.trakt.common.enums.ContentType;

public class ApplicationPreferences {
    private final String CURRENT_VIDEO_CONTENT_TYPE = "CURRENT_VIDEO_CONTENT_TYPE";
    private final String CURRENT_VIDEO_CONTENT_TAG = "CURRENT_VIDEO_CONTENT_TAG";

    protected final SharedPreferences sharedPreferences;

    public ApplicationPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
    }

    public ContentType getCurrentVideoContentType() {
        return ContentType.getByApiKey(sharedPreferences.getString(CURRENT_VIDEO_CONTENT_TYPE, ContentType.MOVIE.getApiKey()));
    }

    public void setCurrentVideoContentType(ContentType contentType) {
        sharedPreferences.edit().putString(CURRENT_VIDEO_CONTENT_TYPE, contentType.getApiKey()).apply();
    }

    public ContentTag getCurrentVideoContentTag() {
        return ContentTag.getByApiKey(sharedPreferences.getString(CURRENT_VIDEO_CONTENT_TAG, ContentTag.TOP_RATED.getApiKey()));
    }

    public void setCurrentVideoContentTag(ContentTag contentTag) {
        sharedPreferences.edit().putString(CURRENT_VIDEO_CONTENT_TAG, contentTag.getApiKey()).apply();
    }
}
