package mdd.endava.trakt;

public class Cryptor {
    public String encrypt(String password) {
        StringBuilder encrypted = new StringBuilder();
        if (password == null) {
            return null;
        }
        for (Character c : password.toCharArray()) {
            encrypted.append(Integer.toString(((int) c) + 13));
            encrypted.append(".");
        }
        return encrypted.substring(0, encrypted.length() - 1);
    }

    public String decrypt(String encPassword) {
        StringBuilder decrypted = new StringBuilder();
        if (encPassword == null) {
            return null;
        }
        String[] splitPass = encPassword.split("\\.");
        for (String aString : splitPass) {
            decrypted.append((char) (Integer.parseInt(aString) - 13));
        }
        return decrypted.toString();
    }
}
