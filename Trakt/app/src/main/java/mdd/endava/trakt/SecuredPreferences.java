package mdd.endava.trakt;

import android.content.Context;

public class SecuredPreferences extends ApplicationPreferences {

    private final String REQUEST_TOKEN = "REQUEST_TOKEN";
    private final String SESSION_ID = "SESSION_ID";
    private final String USERNAME = "CURRENT_USERNAME";
    private final String PASSWORD = "DROWSSAP";
    private final String REMEMBER_CREDENTIALS = "REMEMBER_CREDENTIALS";

    private final Cryptor cryptor;

    public SecuredPreferences(Context context, Cryptor cryptor) {
        super(context);
        this.cryptor = cryptor;
    }

    public void setRequestToken(String requestToken) {
        sharedPreferences.edit().putString(REQUEST_TOKEN, requestToken).apply();
    }

    public String getRequestToken() {
        return sharedPreferences.getString(REQUEST_TOKEN, null);
    }

    public void setSessionId(String sessionId) {
        sharedPreferences.edit().putString(SESSION_ID, sessionId).apply();
    }

    public String getSessionId() {
        return sharedPreferences.getString(SESSION_ID, null);
    }

    public void setUsername(String username) {
        sharedPreferences.edit().putString(USERNAME, username).apply();
    }

    public String getUsername() {
        return sharedPreferences.getString(USERNAME, null);
    }

    public void setPassword(String password) {
        sharedPreferences.edit().putString(PASSWORD, cryptor.encrypt(password)).apply();
    }

    public String getPassword() {
        return cryptor.decrypt(sharedPreferences.getString(PASSWORD, null));
    }

    public void setRememberCredentials(boolean rememberCredentials) {
        sharedPreferences.edit().putBoolean(REMEMBER_CREDENTIALS, rememberCredentials).apply();
    }

    public boolean rememberCredentials() {
        return sharedPreferences.getBoolean(REMEMBER_CREDENTIALS, false);
    }
}
