package mdd.endava.trakt.common;

import mdd.endava.trakt.common.enums.ContentType;

public interface IContentType {
    ContentType contentType();
    int getType();
}
