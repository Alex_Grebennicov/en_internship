package mdd.endava.trakt.common;

public interface IModel<T extends IContentType> {
    T getObject();
}
