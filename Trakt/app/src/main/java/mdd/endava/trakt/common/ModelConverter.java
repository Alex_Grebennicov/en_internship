package mdd.endava.trakt.common;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import mdd.endava.trakt.database.model.MovieDBModel;
import mdd.endava.trakt.database.model.TVShowDBModel;
import mdd.endava.trakt.database.response.MovieDBResponse;
import mdd.endava.trakt.database.response.TVShowDBResponse;

public class ModelConverter {
    public List<IModel> getNeutralMovieList(RealmResults<MovieDBModel> realmResults, int startPos, int endPos) {
        List<IModel> list = new ArrayList<>();
        for (int i = startPos; i < endPos && i < realmResults.size(); i++) {
            final MovieDBResponse movie = new MovieDBResponse(
                    realmResults.get(i).getId(),
                    realmResults.get(i).getTitle(),
                    realmResults.get(i).getThumbImagePath(),
                    realmResults.get(i).getBackdropImage(),
                    realmResults.get(i).getOverview(),
                    realmResults.get(i).getOriginalLanguage(),
                    realmResults.get(i).getPopularity(),
                    realmResults.get(i).getVoteAverage(),
                    realmResults.get(i).isAdult(),
                    realmResults.get(i).getReleaseDate());
            list.add(new IModel() {
                @Override
                public IContentType getObject() {
                    return movie;
                }
            });
        }
        return list;
    }

    public List<IModel> getNeutralTVShowList(RealmResults<TVShowDBModel> realmResults, int startPos, int endPos) {
        List<IModel> list = new ArrayList<>();
        for (int i = startPos; i < endPos && i < realmResults.size(); i++) {
            final TVShowDBResponse tvShow = new TVShowDBResponse(
                    realmResults.get(i).getId(),
                    realmResults.get(i).getTitle(),
                    realmResults.get(i).getThumbImagePath(),
                    realmResults.get(i).getBackdropImage(),
                    realmResults.get(i).getOverview(),
                    realmResults.get(i).getOriginalLanguage(),
                    realmResults.get(i).getPopularity(),
                    realmResults.get(i).getVoteAverage(),
                    realmResults.get(i).getFirstAirDate());
            list.add(new IModel() {
                @Override
                public IContentType getObject() {
                    return tvShow;
                }
            });
        }
        return list;
    }
}
