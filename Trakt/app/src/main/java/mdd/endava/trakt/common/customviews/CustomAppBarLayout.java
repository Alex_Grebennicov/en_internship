package mdd.endava.trakt.common.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.AppBarLayout;
import android.util.AttributeSet;

import mdd.endava.trakt.R;

public class CustomAppBarLayout extends AppBarLayout {

    private int ratio1;
    private int ratio2;

    public CustomAppBarLayout(Context context) {
        super(context);
    }

    public CustomAppBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomAppBarLayout,
                0, 0);
        try {
            ratio1 = a.getInteger(R.styleable.CustomAppBarLayout_ratio1, 2);
            ratio2 = a.getInteger(R.styleable.CustomAppBarLayout_ratio2, 3);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int threeTwoHeight = MeasureSpec.getSize(widthMeasureSpec) * ratio2 / ratio1;
        int threeTwoHeightSpec = MeasureSpec.makeMeasureSpec(threeTwoHeight, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, threeTwoHeightSpec);
    }
}
