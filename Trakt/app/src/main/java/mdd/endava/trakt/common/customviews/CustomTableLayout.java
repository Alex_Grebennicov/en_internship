package mdd.endava.trakt.common.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import mdd.endava.trakt.R;

import static android.support.v4.widget.TextViewCompat.setTextAppearance;

public class CustomTableLayout extends TableLayout {
    public CustomTableLayout(Context context) {
        super(context);
    }

    public CustomTableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void addTableRow(String title, String inputData) {
        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        TableRow.LayoutParams titleParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.3f);
        TableRow.LayoutParams contentParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.7f);

        TableRow tableRow = new TableRow(getContext());
        tableRow.setLayoutParams(rowParams);

        TextView titleCell = new TextView(getContext());
        titleCell.setLayoutParams(titleParams);
        titleCell.setText(title);
        setTextAppearance(titleCell, R.style.TitleCell);

        TextView contentCell = new TextView(getContext());
        contentCell.setLayoutParams(contentParams);
        contentCell.setText(inputData);

        tableRow.addView(titleCell);
        tableRow.addView(contentCell);
        addView(tableRow);
    }
}
