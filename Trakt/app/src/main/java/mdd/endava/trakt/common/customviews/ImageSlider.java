package mdd.endava.trakt.common.customviews;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import mdd.endava.trakt.R;
import mdd.endava.trakt.moviedetails.movieimages.ViewPagerAdapter;

/**
 * Created by cmardari on 19-Oct-17.
 */

public class ImageSlider extends FrameLayout {
    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private ViewPagerAdapter adapter;
    private ImageView[] imageDotsList;

    private int activeImageDotPosition;

    private int numberOfDots;

    public ImageSlider(@NonNull Context context) {
        super(context);
        init();
    }

    public ImageSlider(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ImageSlider(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        inflate(getContext(), R.layout.view_image_slider, this);
        viewPager = findViewById(R.id.backdrops_viewpager);
        dotsLayout = findViewById(R.id.slider_dots);
    }

    public void setAdapter(ViewPagerAdapter adapter) {
        this.adapter = adapter;
        viewPager.setAdapter(adapter);
    }

    public void addDots() {
        activeImageDotPosition = viewPager.getCurrentItem();
        dotsLayout.removeAllViews();
        if (adapter.getCount() != 0) {
            createDots();
        }
    }

    private void createDots() {
        numberOfDots = adapter.getCount();
        imageDotsList = new ImageView[numberOfDots];

        for (int i = 0; i < numberOfDots; i++) {
            imageDotsList[i] = new ImageView(getContext());
            imageDotsList[i].setImageResource(R.drawable.nonactive_dot);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(5, 0, 5, 0);

            dotsLayout.addView(imageDotsList[i], params);
        }
        imageDotsList[activeImageDotPosition].setImageResource(R.drawable.active_dot);
        viewPager.addOnPageChangeListener(onPageChangeListener);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            activeImageDotPosition = position;
            for (int i = 0; i < numberOfDots; i++) {
                imageDotsList[i].setImageResource(R.drawable.nonactive_dot);
            }
            imageDotsList[position].setImageResource(R.drawable.active_dot);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    };
}
