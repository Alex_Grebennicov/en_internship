package mdd.endava.trakt.common.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import mdd.endava.trakt.R;

import static android.support.v4.widget.TextViewCompat.setTextAppearance;

public class RatingWidget extends LinearLayout {
    float rating;

    public RatingWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public RatingWidget(Context context) {
        super(context);
        init();
    }

    public RatingWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setGravity(Gravity.CENTER_VERTICAL);
        setOrientation(LinearLayout.HORIZONTAL);
        create();
    }

    public void setRating(float rating) {
        this.rating = rating;
        if (getChildCount() > 0) {
            removeAllViews();
        }
        create();
    }

    private void create() {
        ImageView starImgView;
        int fullStars = (int) rating / 2;
        int halfStars;
        if ((rating % 2) >= 1.001) {
            halfStars = 0;
            fullStars++;
        } else {
            halfStars = 1;
        }
        int emptyStars = 5 - fullStars - halfStars;

        for (int i = 0; i < fullStars; i++) {
            starImgView = new ImageView(getContext());
            starImgView.setImageResource(R.drawable.ic_star_black_18dp);
            addView(starImgView);
        }

        if (halfStars == 1) {
            starImgView = new ImageView(getContext());
            starImgView.setImageResource(R.drawable.ic_star_half_black_18dp);
            addView(starImgView);
        }

        for (int i = 0; i < emptyStars; i++) {
            starImgView = new ImageView(getContext());
            starImgView.setImageResource(R.drawable.ic_star_border_black_18dp);
            addView(starImgView);
        }

        TextView textView = new TextView(getContext());
        setTextAppearance(textView, R.style.BasicDetailsMovieTextView);
        textView.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        textView.setPadding(getContext().getResources().getDimensionPixelOffset(R.dimen.common_4dp), 0, 0, 0);
        textView.setText(String.valueOf(new DecimalFormat("##.##").format(rating)));
        addView(textView);
    }


}
