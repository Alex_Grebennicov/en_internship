package mdd.endava.trakt.common.enums;

import mdd.endava.trakt.R;

public enum AppError {
    NO_NETWORK(R.string.error_no_network),
    SERVER_ERROR(R.string.error_on_server),
    INVALID_CREDENTIALS(R.string.error_wrong_credentials),
    SYSTEM_ERROR(R.string.error_system);

    private int errorMessageResID;

    AppError(int errorMessageResID) {
        this.errorMessageResID = errorMessageResID;
    }

    public int errorMessageResID() {
        return errorMessageResID;
    }
}
