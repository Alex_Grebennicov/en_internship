package mdd.endava.trakt.common.enums;

import mdd.endava.trakt.R;

import static mdd.endava.trakt.home.HomeActivity.BOTTOMBAR_POSITION_FAVORITES;
import static mdd.endava.trakt.home.HomeActivity.BOTTOMBAR_POSITION_POPULAR;
import static mdd.endava.trakt.home.HomeActivity.BOTTOMBAR_POSITION_TOP_RATED;

public enum ContentTag {

    TOP_RATED(BOTTOMBAR_POSITION_TOP_RATED, "top_rated", R.string.top_rated),
    POPULAR(BOTTOMBAR_POSITION_POPULAR, "popular", R.string.popular),
    FAVORITES(BOTTOMBAR_POSITION_FAVORITES, "favorites", R.string.favorites);

    private int bottombarPosition;
    private String apiKey;
    private int title;

    ContentTag(int bottombarPosition, String apiKey, int title) {
        this.bottombarPosition = bottombarPosition;
        this.apiKey = apiKey;
        this.title = title;
    }

    public int getBottombarPosition() {
        return bottombarPosition;
    }

    public String getApiKey() {
        return apiKey;
    }

    public int getTitle() {
        return title;
    }

    public static ContentTag getByApiKey(String apiKey) {
        if (apiKey == null) {
            return null;
        }
        for (ContentTag tag : ContentTag.values()) {
            if (apiKey.equals(tag.getApiKey())) {
                return tag;
            }
        }
        return null;
    }
}
