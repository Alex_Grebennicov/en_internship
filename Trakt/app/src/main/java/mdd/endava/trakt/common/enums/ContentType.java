package mdd.endava.trakt.common.enums;

import static mdd.endava.trakt.home.HomeActivity.SPINNER_POSITION_MOVIE;
import static mdd.endava.trakt.home.HomeActivity.SPINNER_POSITION_TVSHOW;

public enum ContentType {

    MOVIE("movie", SPINNER_POSITION_MOVIE),
    TV_SHOW("tv", SPINNER_POSITION_TVSHOW);

    private String apiKey;
    private int spinnerPosition;

    ContentType(String apiKey, int spinnerPosition) {
        this.apiKey = apiKey;
        this.spinnerPosition = spinnerPosition;
    }

    public String getApiKey() {
        return apiKey;
    }

    public int getSpinnerPosition() {
        return spinnerPosition;
    }

    public static ContentType getByApiKey(String apiKey) {
        if (apiKey == null) {
            return null;
        }
        for (ContentType type : ContentType.values()) {
            if (apiKey.equals(type.getApiKey())) {
                return type;
            }
        }
        return null;
    }
}
