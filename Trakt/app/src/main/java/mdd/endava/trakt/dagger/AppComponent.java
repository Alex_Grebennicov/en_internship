package mdd.endava.trakt.dagger;

import android.content.Context;

import dagger.Component;
import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.dagger.modules.DataAccessModule;
import mdd.endava.trakt.dagger.modules.NetworkModule;
import mdd.endava.trakt.dagger.modules.UtilModule;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.MovieDBAPI;

@ApplicationScope
@Component(modules = {NetworkModule.class, DataAccessModule.class, UtilModule.class})
public interface AppComponent {

    Context getContext();

    Device getDevice();

    MovieDBAPI getAPI();

    ApplicationPreferences getPreferences();

    SecuredPreferences getSecuredPreferences();
}
