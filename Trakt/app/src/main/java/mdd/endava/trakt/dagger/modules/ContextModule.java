package mdd.endava.trakt.dagger.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.dagger.ApplicationScope;

@Module
public class ContextModule {
    private final Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @ApplicationScope
    @Provides
    public Context context() {
        return context;
    }
}
