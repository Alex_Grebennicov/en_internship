package mdd.endava.trakt.dagger.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.Cryptor;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.dagger.ApplicationScope;

@Module(includes = ContextModule.class)
public class DataAccessModule {
    @ApplicationScope
    @Provides
    public ApplicationPreferences preferences(Context context) {
        return new ApplicationPreferences(context);
    }

    @ApplicationScope
    @Provides
    public SecuredPreferences securedPreferences(Context context, Cryptor cryptor) {
        return new SecuredPreferences(context, cryptor);
    }

    @ApplicationScope
    @Provides
    public Cryptor cryptor() {
        return new Cryptor();
    }
}
