package mdd.endava.trakt.dagger.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.R;
import mdd.endava.trakt.dagger.ApplicationScope;
import mdd.endava.trakt.network.MovieDBAPI;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ContextModule.class)
public class NetworkModule {
    @ApplicationScope
    @Provides
    public MovieDBAPI api(Context context) {
        return (new Retrofit.Builder())
                .baseUrl(context.getResources().getString(R.string.api_base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(MovieDBAPI.class);
    }
}
