package mdd.endava.trakt.dagger.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.dagger.ApplicationScope;
import mdd.endava.trakt.network.Device;

@Module(includes = ContextModule.class)
public class UtilModule {
    @ApplicationScope
    @Provides
    public Device device(Context context) {
        return new Device(context);
    }
}
