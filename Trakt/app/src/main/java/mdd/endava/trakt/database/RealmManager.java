package mdd.endava.trakt.database;

import io.realm.Realm;
import mdd.endava.trakt.database.newdao.MovieDao;
import mdd.endava.trakt.database.newdao.MovieDetailsDao;
import mdd.endava.trakt.database.newdao.TvshowDao;
import mdd.endava.trakt.database.newdao.TvshowDetailsDao;

public class RealmManager {

    private Realm mRealm;

    public Realm open() {
        mRealm = Realm.getDefaultInstance();
        return mRealm;
    }

    public void close() {
        if (mRealm != null) {
            mRealm.close();
        }
    }

    public MovieDetailsDao createMovieDetailsDao() {
        checkForOpenRealm();
        return new MovieDetailsDao(mRealm);
    }

    public MovieDao createMovieDao() {
        checkForOpenRealm();
        return new MovieDao(mRealm);
    }

    public TvshowDetailsDao createTvshowDetailsDao() {
        checkForOpenRealm();
        return new TvshowDetailsDao(mRealm);
    }

    public TvshowDao createTvshowDao() {
        checkForOpenRealm();
        return new TvshowDao(mRealm);
    }

    private void checkForOpenRealm() {
        if (mRealm == null || mRealm.isClosed()) {
            throw new IllegalStateException("RealmManager: Realm is closed, call open() method first");
        }
    }
}
