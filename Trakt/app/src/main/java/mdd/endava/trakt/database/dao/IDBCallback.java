package mdd.endava.trakt.database.dao;

public interface IDBCallback {
    void onTransactionDone();
}
