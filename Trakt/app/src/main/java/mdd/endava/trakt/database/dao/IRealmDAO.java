package mdd.endava.trakt.database.dao;

import java.util.List;

import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.ContentTag;
import retrofit2.Response;

public interface IRealmDAO<RetrofitObject> {
    void resetPopularity();

    void resetRating();

    List<IModel> getViewModels(int page, ContentTag contentTag);

    void insertOrUpdateParsedObjects(Response<RetrofitObject> response, IDBCallback databaseCallback);
}
