package mdd.endava.trakt.database.dao;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.ModelConverter;
import mdd.endava.trakt.common.enums.ContentTag;
import mdd.endava.trakt.database.model.MovieDBModel;
import mdd.endava.trakt.network.MovieDBAPI;
import mdd.endava.trakt.network.models.movies.ParsedMovie;
import mdd.endava.trakt.network.models.movies.ParsedMovieList;
import retrofit2.Response;

public class MovieRealmDAO implements IRealmDAO<ParsedMovieList> {

    private final ModelConverter modelConverter;

    public MovieRealmDAO() {
        modelConverter = new ModelConverter();
    }

    @Override
    public void resetPopularity() {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<MovieDBModel> results = realm.where(MovieDBModel.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (MovieDBModel movie : results) {
                    movie.setPopularity(0f);
                }
            }
        });
        realm.close();
    }

    @Override
    public void resetRating() {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<MovieDBModel> results = realm.where(MovieDBModel.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (MovieDBModel movie : results) {
                    movie.setVoteAverage(0f);
                }
            }
        });
        realm.close();
    }

    @Override
    public List<IModel> getViewModels(int page, ContentTag contentTag) {
        switch (contentTag) {
            case POPULAR:
                return getPopularViewModels(page);
            case TOP_RATED:
                return getTopRatedViewModels(page);
            default:
                return new ArrayList<>();
        }
    }

    @Override
    public void insertOrUpdateParsedObjects(Response<ParsedMovieList> response, IDBCallback databaseCallback) {
        final List<MovieDBModel> movieList = new ArrayList<>();
        for (ParsedMovie parsedMovie : response.body().getResults()) {
            MovieDBModel movie = new MovieDBModel();
            movie.setId(parsedMovie.getId());
            movie.setTitle(parsedMovie.getTitle());
            movie.setThumbImagePath(parsedMovie.getThumbPathSuffix());
            movie.setOverview(parsedMovie.getOverview());
            movie.setOriginalLanguage(parsedMovie.getOriginalLanguage());
            movie.setPopularity(parsedMovie.getPopularity());
            movie.setVoteAverage(parsedMovie.getVoteAverage());
            movie.setAdult(parsedMovie.isAdult());
            movie.setReleaseDate(parsedMovie.getReleaseDate());
            movie.setBackdropImage(parsedMovie.getBackdropImagePathSuffix());
            movieList.add(movie);
        }
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(movieList);
            }
        });
        realm.close();
        databaseCallback.onTransactionDone();
    }

    private List<IModel> getPopularViewModels(final int page) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<MovieDBModel> results = realm.where(MovieDBModel.class)
                .notEqualTo("popularity", 0f)
                .findAllSorted("popularity", Sort.DESCENDING);
        List<IModel> list = modelConverter.getNeutralMovieList(results, MovieDBAPI.ITEMS_PER_PAGE * (page - 1), MovieDBAPI.ITEMS_PER_PAGE * page);
        realm.close();
        return list;
    }

    private List<IModel> getTopRatedViewModels(int page) {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<MovieDBModel> results = realm.where(MovieDBModel.class)
                .notEqualTo("voteAverage", 0f)
                .findAllSorted("voteAverage", Sort.DESCENDING);
        List<IModel> list = modelConverter.getNeutralMovieList(results, MovieDBAPI.ITEMS_PER_PAGE * (page - 1), MovieDBAPI.ITEMS_PER_PAGE * page);
        realm.close();
        return list;
    }
}
