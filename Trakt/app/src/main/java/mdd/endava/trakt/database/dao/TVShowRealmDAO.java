package mdd.endava.trakt.database.dao;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.ModelConverter;
import mdd.endava.trakt.common.enums.ContentTag;
import mdd.endava.trakt.database.model.TVShowDBModel;
import mdd.endava.trakt.network.MovieDBAPI;
import mdd.endava.trakt.network.models.tvshows.ParsedTVShow;
import mdd.endava.trakt.network.models.tvshows.ParsedTVShowList;
import retrofit2.Response;

public class TVShowRealmDAO implements IRealmDAO<ParsedTVShowList> {

    private final ModelConverter modelConverter;

    public TVShowRealmDAO() {
        this.modelConverter = new ModelConverter();
    }

    @Override
    public void resetPopularity() {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<TVShowDBModel> results = realm.where(TVShowDBModel.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (TVShowDBModel show : results) {
                    show.setPopularity(0f);
                }
            }
        });
        realm.close();
    }

    @Override
    public void resetRating() {
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<TVShowDBModel> results = realm.where(TVShowDBModel.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (TVShowDBModel show : results) {
                    show.setVoteAverage(0f);
                }
            }
        });
        realm.close();
    }

    @Override
    public List<IModel> getViewModels(int page, ContentTag contentTag) {
        switch (contentTag) {
            case POPULAR:
                return getPopularViewModels(page);
            case TOP_RATED:
                return getTopRatedViewModels(page);
            default:
                return new ArrayList<>();
        }
    }

    @Override
    public void insertOrUpdateParsedObjects(Response<ParsedTVShowList> response, IDBCallback databaseCallback) {
        final List<TVShowDBModel> tvShowList = new ArrayList<>();
        for (ParsedTVShow parsedTVShow : response.body().getResults()) {
            TVShowDBModel tvShow = new TVShowDBModel();
            tvShow.setId(parsedTVShow.getId());
            tvShow.setTitle(parsedTVShow.getTitle());
            tvShow.setThumbImagePath(parsedTVShow.getThumbPathSuffix());
            tvShow.setBackdropImage(parsedTVShow.getBackdropImagePathSuffix());
            tvShow.setOverview(parsedTVShow.getOverview());
            tvShow.setOriginalLanguage(parsedTVShow.getOriginalLanguage());
            tvShow.setPopularity(parsedTVShow.getPopularity());
            tvShow.setVoteAverage(parsedTVShow.getVoteAverage());
            tvShow.setFirstAirDate(parsedTVShow.getFirstAirDate());
            tvShowList.add(tvShow);
        }
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(tvShowList);
            }
        });
        realm.close();
        databaseCallback.onTransactionDone();
    }

    private List<IModel> getPopularViewModels(int page) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<TVShowDBModel> results = realm.where(TVShowDBModel.class)
                .notEqualTo("popularity", 0f)
                .findAllSorted("popularity", Sort.DESCENDING);
        ModelConverter modelConverter = new ModelConverter();
        List<IModel> list = modelConverter.getNeutralTVShowList(results, MovieDBAPI.ITEMS_PER_PAGE * (page - 1), MovieDBAPI.ITEMS_PER_PAGE * page);
        realm.close();
        return list;
    }

    private List<IModel> getTopRatedViewModels(int page) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<TVShowDBModel> results = realm.where(TVShowDBModel.class)
                .notEqualTo("voteAverage", 0f)
                .findAllSorted("voteAverage", Sort.DESCENDING);
        List<IModel> list = modelConverter.getNeutralTVShowList(results, MovieDBAPI.ITEMS_PER_PAGE * (page - 1), MovieDBAPI.ITEMS_PER_PAGE * page);
        realm.close();
        return list;
    }
}
