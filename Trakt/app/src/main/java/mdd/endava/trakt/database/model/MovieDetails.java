package mdd.endava.trakt.database.model;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import mdd.endava.trakt.models.moviedetails.Genre;
import mdd.endava.trakt.models.moviedetails.ProductionCountry;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.moviepeople.Cast;

public class MovieDetails extends RealmObject {

    @PrimaryKey
    private long id;
    //private int videoType;

    private String homepage;
    private String youtubeId;
    private int runtimeMin;
    private String tagline;
    private String imdbId;
    private RealmList<Genre> genreList = new RealmList<>();

    private RealmList<ProductionCountry> prodCountriesList = new RealmList<>();
    private RealmList<Cast> castList = new RealmList<>();
    private RealmList<Backdrop> backdropsList = new RealmList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RealmList<Backdrop> getBackdropsList() {
        return backdropsList;
    }

    public void setBackdropsList(List<Backdrop> backdropsList) {
        this.backdropsList.clear();
        this.backdropsList.addAll(backdropsList);
    }

    public void setProdCountriesList(List<ProductionCountry> list) {
        prodCountriesList.clear();
        prodCountriesList.addAll(list);
    }

    public void setGenreList(List<Genre> list) {
        genreList.clear();
        genreList.addAll(list);
    }

    public RealmList<ProductionCountry> getProdCountriesList() {
        return prodCountriesList;
    }

    public RealmList<Genre> getGenreList() {
        return genreList;
    }

    public RealmList<Cast> getCastList() {
        return castList;
    }

    public void setCastList(List<Cast> castList) {
        this.castList.clear();
        this.castList.addAll(castList);
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
        this.youtubeId = youtubeId;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public int getRuntimeMin() {
        return runtimeMin;
    }

    public void setRuntimeMin(int runtimeMin) {
        this.runtimeMin = runtimeMin;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

}
