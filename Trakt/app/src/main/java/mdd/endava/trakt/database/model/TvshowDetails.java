package mdd.endava.trakt.database.model;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.moviepeople.Cast;
import mdd.endava.trakt.models.tvdetails.Season;

/**
 * Created by User on 05.11.2017.
 */

public class TvshowDetails extends RealmObject {
    @PrimaryKey
    private long id;

    private String homepage;
    private String trailer;
    private String runtime;
    private String genres;
    private RealmList<Cast> castList = new RealmList<>();
    private RealmList<Backdrop> backdropsList = new RealmList<>();
    private String originCountries;
    private String firstAirDate;
    private String status;
    private String networks;
    private RealmList<Season> seasons = new RealmList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public RealmList<Cast> getCastList() {
        return castList;
    }

    public void setCastList(List<Cast> castList) {
        this.castList.clear();
        this.castList.addAll(castList);
    }

    public RealmList<Backdrop> getBackdropsList() {
        return backdropsList;
    }

    public void setBackdropsList(List<Backdrop> backdropsList) {
        this.backdropsList.clear();
        this.backdropsList.addAll(backdropsList);
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public void setFirstAirDate(String firstAirDate) {
        this.firstAirDate = firstAirDate;
    }

    public String getOriginCountries() {
        return originCountries;
    }

    public void setOriginCountries(String originCountries) {
        this.originCountries = originCountries;
    }

    public String getNetworks() {
        return networks;
    }

    public void setNetworks(String networks) {
        this.networks = networks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons.clear();
        this.seasons.addAll(seasons);
    }
}
