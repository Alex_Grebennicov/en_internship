package mdd.endava.trakt.database.newdao;

import io.realm.Realm;
import mdd.endava.trakt.database.model.MovieDBModel;

public class MovieDao {

    private Realm realm;

    public MovieDao(Realm realm) {
        this.realm = realm;
    }

    public MovieDBModel getById(long id) {
        return realm.where(MovieDBModel.class).equalTo("id", id).findFirst();
    }
}
