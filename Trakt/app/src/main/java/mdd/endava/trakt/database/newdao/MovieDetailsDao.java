package mdd.endava.trakt.database.newdao;

import io.realm.Realm;
import mdd.endava.trakt.database.model.MovieDetails;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.MovieDataViewModel;

public class MovieDetailsDao {

    private Realm realm;

    public MovieDetailsDao(Realm realm) {
        this.realm = realm;
    }

    public MovieDetails getById(long id) {
        return realm.where(MovieDetails.class).equalTo("id", id).findFirst();
    }

    public void save(long id, DataViewModel data) {
        MovieDataViewModel movieData = (MovieDataViewModel) data;

        realm.beginTransaction();

        MovieDetails movieDetails = realm.where(MovieDetails.class).equalTo("id", id).findFirst();
        if (movieDetails == null) {
            movieDetails = realm.createObject(MovieDetails.class, id);
        }

        movieDetails.setTagline(movieData.getTagline());
        movieDetails.setRuntimeMin(movieData.getRuntimeMin());
        movieDetails.setImdbId(movieData.getImdbId());
        movieDetails.setHomepage(movieData.getHomepageUrl());
        movieDetails.setYoutubeId(movieData.getYoutubeId());
        movieDetails.setBackdropsList(movieData.getBackdropList());
        if (movieData.getGenresList() != null) {
            movieDetails.setGenreList(movieData.getGenresList());
        }
        if (movieData.getProdCountriesList() != null) {
            movieDetails.setProdCountriesList(movieData.getProdCountriesList());
        }
        if (movieData.getCastList() != null) {
            movieDetails.setCastList(movieData.getCastList());
        }

        realm.insertOrUpdate(movieDetails);
        realm.commitTransaction();


    }
}
