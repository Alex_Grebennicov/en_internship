package mdd.endava.trakt.database.newdao;

import io.realm.Realm;
import mdd.endava.trakt.database.model.TVShowDBModel;

public class TvshowDao {

    private Realm realm;

    public TvshowDao(Realm realm) {
        this.realm = realm;
    }

    public TVShowDBModel getById(long id) {
        return realm.where(TVShowDBModel.class).equalTo("id", id).findFirst();
    }
}
