package mdd.endava.trakt.database.newdao;

import io.realm.Realm;
import mdd.endava.trakt.database.model.TvshowDetails;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.TvshowDataViewModel;

public class TvshowDetailsDao {

    private Realm realm;

    public TvshowDetailsDao(Realm realm) {
        this.realm = realm;
    }

    public TvshowDetails getById(long id) {
        return realm.where(TvshowDetails.class).equalTo("id", id).findFirst();
    }

    public void save(long id, DataViewModel data) {
        TvshowDataViewModel tvshowData = (TvshowDataViewModel) data;

        realm.beginTransaction();

        TvshowDetails tvshowDetails = realm.where(TvshowDetails.class).equalTo("id", id).findFirst();
        if (tvshowDetails == null) {
            tvshowDetails = realm.createObject(TvshowDetails.class, id);
        }

        tvshowDetails.setRuntime(tvshowData.getRuntime());
        tvshowDetails.setHomepage(tvshowData.getHomepageUrl());
        tvshowDetails.setTrailer(tvshowData.getTrailerUrl());
        tvshowDetails.setBackdropsList(tvshowData.getBackdropList());
        tvshowDetails.setGenres(tvshowData.getGenres());
        if (tvshowData.getOriginCountries() != null) {
            tvshowDetails.setOriginCountries(tvshowData.getOriginCountries());
        }
        if (tvshowData.getCastList() != null) {
            tvshowDetails.setCastList(tvshowData.getCastList());
        }
        if (tvshowData.getSeasons() != null) {
            tvshowDetails.setSeasons(tvshowData.getSeasons());
        }
        tvshowDetails.setNetworks(tvshowData.getNetworks());
        tvshowDetails.setFirstAirDate(tvshowData.getFirstAirDate());
        tvshowDetails.setStatus(tvshowData.getStatus());

        realm.insertOrUpdate(tvshowDetails);
        realm.commitTransaction();

    }
}
