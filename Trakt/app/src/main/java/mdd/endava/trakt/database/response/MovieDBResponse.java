package mdd.endava.trakt.database.response;

import mdd.endava.trakt.common.IContentType;
import mdd.endava.trakt.common.enums.ContentType;

import static mdd.endava.trakt.home.adapter.HomeRecyclerAdapter.VIEW_TYPE_MOVIE;

public class MovieDBResponse implements IContentType {
    private final long id;
    private final String title;
    private final String thumbImagePath;
    private final String backdropImage;
    private final String overview;
    private final String originalLanguage;
    private final float popularity;
    private final float voteAverage;
    private final boolean adult;
    private final String releaseDate;

    public MovieDBResponse(long id, String title, String thumbImagePath, String backdropImage, String overview, String originalLanguage, float popularity, float voteAverage, boolean adult, String releaseDate) {
        this.id = id;
        this.title = title;
        this.thumbImagePath = thumbImagePath;
        this.backdropImage = backdropImage;
        this.overview = overview;
        this.originalLanguage = originalLanguage;
        this.popularity = popularity;
        this.voteAverage = voteAverage;
        this.adult = adult;
        this.releaseDate = releaseDate;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbImagePath() {
        return thumbImagePath;
    }

    public String getBackdropImage() {
        return backdropImage;
    }

    public String getOverview() {
        return overview;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public float getPopularity() {
        return popularity;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public boolean isAdult() {
        return adult;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @Override
    public ContentType contentType() {
        return ContentType.MOVIE;
    }

    @Override
    public int getType() {
        return VIEW_TYPE_MOVIE;
    }
}
