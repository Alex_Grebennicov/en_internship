package mdd.endava.trakt.database.response;

import mdd.endava.trakt.common.IContentType;
import mdd.endava.trakt.common.enums.ContentType;

import static mdd.endava.trakt.home.adapter.HomeRecyclerAdapter.VIEW_TYPE_TV_SHOW;

public class TVShowDBResponse implements IContentType {
    private final long id;
    private final String title;
    private final String thumbImagePath;
    private final String backdropImage;
    private final String overview;
    private final String originalLanguage;
    private final float popularity;
    private final float voteAverage;
    private final String firstAirDate;

    public TVShowDBResponse(long id, String title, String thumbImagePath, String backdropImage, String overview, String originalLanguage, float popularity, float voteAverage, String firstAirDate) {
        this.id = id;
        this.title = title;
        this.thumbImagePath = thumbImagePath;
        this.backdropImage = backdropImage;
        this.overview = overview;
        this.originalLanguage = originalLanguage;
        this.popularity = popularity;
        this.voteAverage = voteAverage;
        this.firstAirDate = firstAirDate;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbImagePath() {
        return thumbImagePath;
    }

    public String getBackdropImage() {
        return backdropImage;
    }

    public String getOverview() {
        return overview;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public float getPopularity() {
        return popularity;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    @Override
    public ContentType contentType() {
        return ContentType.TV_SHOW;
    }

    @Override
    public int getType() {
        return VIEW_TYPE_TV_SHOW;
    }
}
