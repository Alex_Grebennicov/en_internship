package mdd.endava.trakt.favorites;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mdd.endava.trakt.BaseActivity;
import mdd.endava.trakt.R;


public class FavoritesFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((BaseActivity)getActivity()).setToolBar(getString(R.string.favorites));
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }
}
