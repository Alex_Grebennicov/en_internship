package mdd.endava.trakt.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import mdd.endava.trakt.App;
import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.BaseActivity;
import mdd.endava.trakt.R;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.common.enums.ContentTag;
import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.favorites.FavoritesFragment;
import mdd.endava.trakt.home.view.HomeFragment;
import mdd.endava.trakt.login.LoginActivity;

public class HomeActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {

    public static final int SPINNER_POSITION_MOVIE = 0;
    public static final int SPINNER_POSITION_TVSHOW = 1;
    public static final int BOTTOMBAR_POSITION_TOP_RATED = 0;
    public static final int BOTTOMBAR_POSITION_POPULAR = 1;
    public static final int BOTTOMBAR_POSITION_FAVORITES = 2;
    private ApplicationPreferences preferences;
    private SecuredPreferences securedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        securedPreferences = App.getComponent().getSecuredPreferences();
        preferences = App.getComponent().getPreferences();
        ((Spinner) findViewById(R.id.spinner_actionbar)).setOnItemSelectedListener(this);
        ((BottomNavigationView) findViewById(R.id.bottom_bar)).setOnNavigationItemSelectedListener(this);
        updateNavigationBarState();
        updateSpinner();
        setToolBar(getString(preferences.getCurrentVideoContentTag().getTitle()));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_home;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.top_rated: {
                preferences.setCurrentVideoContentTag(ContentTag.TOP_RATED);
                setupFragment();
                break;
            }
            case R.id.popular: {
                preferences.setCurrentVideoContentTag(ContentTag.POPULAR);
                setupFragment();
                break;
            }

            case R.id.favorites: {
                preferences.setCurrentVideoContentTag(ContentTag.FAVORITES);
                setupFavoriteFragment();
                break;
            }
        }
        return true;
    }

    public void logOutClick(MenuItem menuItem) {
        securedPreferences.setSessionId(null);
        securedPreferences.setRequestToken(null);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getSelectedItemPosition()) {
            case SPINNER_POSITION_MOVIE:
                preferences.setCurrentVideoContentType(ContentType.MOVIE);
                break;
            case SPINNER_POSITION_TVSHOW:
                preferences.setCurrentVideoContentType(ContentType.TV_SHOW);
                break;
        }
        if (preferences.getCurrentVideoContentTag() == ContentTag.FAVORITES) {
            setupFavoriteFragment();
        } else {
            setupFragment();
        }
    }

    private void setupFavoriteFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.list_container, new FavoritesFragment())
                .commit();
    }

    private void setupFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.list_container, new HomeFragment())
                .commit();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void updateNavigationBarState() {
        ContentTag selectedTag = preferences.getCurrentVideoContentTag();
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_bar);
        bottomNavigationView.getMenu().getItem(selectedTag.getBottombarPosition()).setChecked(true);
    }

    private void updateSpinner() {
        ContentType selectedType = preferences.getCurrentVideoContentType();
        Spinner spinner = findViewById(R.id.spinner_actionbar);
        spinner.setSelection(selectedType.getSpinnerPosition());
    }
}