package mdd.endava.trakt.home.adapter;

import java.util.List;

import mdd.endava.trakt.common.IModel;

public interface HomeAdapter {
    void refreshDataList(List<IModel> list);

    void addToDataList(List<IModel> list);

    int getDataSize();
}
