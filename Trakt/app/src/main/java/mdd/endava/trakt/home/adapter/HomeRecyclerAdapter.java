package mdd.endava.trakt.home.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import mdd.endava.trakt.R;
import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.home.viewholders.AbstractViewHolder;
import mdd.endava.trakt.home.viewholders.MovieListItemHolder;
import mdd.endava.trakt.home.viewholders.TVShowListItemHolder;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<AbstractViewHolder> implements HomeAdapter {

    public static final int VIEW_TYPE_MOVIE = 0;
    public static final int VIEW_TYPE_TV_SHOW = 1;

    private List<IModel> dataList;

    public HomeRecyclerAdapter(List<IModel> dataList) {
        this.dataList = dataList;
    }

    @Override
    public int getItemViewType(int position) {
        return dataList.get(position).getObject().getType();
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (VIEW_TYPE_MOVIE == viewType) {
            return new MovieListItemHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.movie_card_view, parent, false));
        } else if (VIEW_TYPE_TV_SHOW == viewType) {
            return new TVShowListItemHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tv_show_card_view, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(AbstractViewHolder holder, int position) {
        holder.bindData(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void refreshDataList(List<IModel> list) {
        dataList = list;
        notifyDataSetChanged();
    }

    @Override
    public void addToDataList(List<IModel> list) {
        dataList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getDataSize() {
        return dataList.size();
    }
}
