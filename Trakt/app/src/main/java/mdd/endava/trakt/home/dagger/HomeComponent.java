package mdd.endava.trakt.home.dagger;

import dagger.Component;
import mdd.endava.trakt.dagger.AppComponent;
import mdd.endava.trakt.home.presenter.HomePresenter;

@HomeScope
@Component(modules = HomeModule.class, dependencies = AppComponent.class)
public interface HomeComponent {
    HomePresenter getPresenter();
}
