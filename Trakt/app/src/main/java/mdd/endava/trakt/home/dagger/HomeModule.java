package mdd.endava.trakt.home.dagger;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.database.dao.IRealmDAO;
import mdd.endava.trakt.database.dao.MovieRealmDAO;
import mdd.endava.trakt.database.dao.TVShowRealmDAO;
import mdd.endava.trakt.home.presenter.HomePresenter;
import mdd.endava.trakt.home.presenter.HomePresenterImpl;
import mdd.endava.trakt.home.repository.HomeAbstractRepository;
import mdd.endava.trakt.home.repository.HomeMovieRepository;
import mdd.endava.trakt.home.repository.HomeTVShowRepository;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.MovieDBAPI;

@Module
public class HomeModule {

    @HomeScope
    @Provides
    public HomePresenter presenter(HomeAbstractRepository repository, Device device) {
        return new HomePresenterImpl(repository, device);
    }

    @Provides
    public HomeAbstractRepository repository(IRealmDAO realmDAO, ApplicationPreferences preferences, MovieDBAPI api) {
        switch (preferences.getCurrentVideoContentType()) {
            case MOVIE:
                return new HomeMovieRepository(realmDAO, preferences, api);
            case TV_SHOW:
                return new HomeTVShowRepository(realmDAO, preferences, api);
            default:
                return null;
        }
    }

    @Provides
    public IRealmDAO realmDAO(ApplicationPreferences preferences) {
        switch (preferences.getCurrentVideoContentType()) {
            case TV_SHOW:
                return new TVShowRealmDAO();
            case MOVIE:
                return new MovieRealmDAO();
            default:
                return null;
        }
    }
}
