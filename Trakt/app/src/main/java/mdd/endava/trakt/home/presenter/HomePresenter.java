package mdd.endava.trakt.home.presenter;

import mdd.endava.trakt.home.view.HomeView;

public interface HomePresenter {
    int PAGE_NUMBER_1 = 1;

    void onViewStart(HomeView view);

    void onViewStop();

    void onViewRefresh();

    void onViewScrollEnd(int adapterDataSize);
}
