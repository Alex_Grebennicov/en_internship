package mdd.endava.trakt.home.presenter;

import android.support.annotation.NonNull;

import java.util.List;

import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.home.repository.HomeAbstractRepository;
import mdd.endava.trakt.home.repository.HomeRepositoryCallback;
import mdd.endava.trakt.home.view.HomeView;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.MovieDBAPI;

public class HomePresenterImpl implements HomePresenter, HomeRepositoryCallback {

    private HomeView view;
    private HomeAbstractRepository repository;
    private Device device;

    public HomePresenterImpl(HomeAbstractRepository repository, Device device) {
        this.repository = repository;
        this.device = device;
    }

    @Override
    public void onViewStart(HomeView view) {
        this.view = view;
        onViewRefresh();
    }

    @Override
    public void onViewStop() {
        this.view = null;
    }

    @Override
    public void onViewRefresh() {
        repository.onRequestData(device.isOnline(), PAGE_NUMBER_1, this);
    }

    @Override
    public void onViewScrollEnd(int adapterDataSize) {
        if (view != null) {
            view.showProgress();
            repository.onRequestData(device.isOnline(), nextPageNumberByDataSize(adapterDataSize), this);
        }
    }

    @Override
    public void onErrorCaught(AppError error) {
        if (view != null) {
            view.showError(error);
        }
    }

    @Override
    public void onListReceived(int page, @NonNull List<IModel> list) {
        if (view == null) {
            return;
        }
        if (!list.isEmpty() && page == PAGE_NUMBER_1) {
            view.refreshDataList(list);
        } else if (list.isEmpty() && !device.isOnline()) {
            view.showError(AppError.NO_NETWORK);
        } else {
            view.addToDataList(list);
        }
    }

    private int nextPageNumberByDataSize(int dataSize) {
        return dataSize / MovieDBAPI.ITEMS_PER_PAGE + 1;
    }
}
