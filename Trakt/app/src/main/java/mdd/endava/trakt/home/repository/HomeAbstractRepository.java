package mdd.endava.trakt.home.repository;

import android.support.annotation.NonNull;

import java.net.UnknownHostException;

import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.common.enums.ContentTag;
import mdd.endava.trakt.database.dao.IDBCallback;
import mdd.endava.trakt.database.dao.IRealmDAO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class HomeAbstractRepository<RetrofitParsedObject> implements Callback<RetrofitParsedObject>, IDBCallback {
    private final IRealmDAO<RetrofitParsedObject> realmDAO;
    private ContentTag contentTag;
    private int page;
    private HomeRepositoryCallback callback;

    HomeAbstractRepository(IRealmDAO<RetrofitParsedObject> realmDAO, ApplicationPreferences preferences) {
        this.realmDAO = realmDAO;
        this.contentTag = preferences.getCurrentVideoContentTag();
    }

    abstract void getDataFromWeb();

    public final void onRequestData(boolean isDeviceOnline, int page, HomeRepositoryCallback callback) {
        this.page = page;
        this.callback = callback;
        if (isDeviceOnline) {
            getDataFromWeb();
        } else {
            getDataFromLocalStorage();
        }
    }

    private void getDataFromLocalStorage() {
        if (getContentTag() == null) {
            callback.onErrorCaught(AppError.SYSTEM_ERROR);
        } else {
            callback.onListReceived(page, realmDAO.getViewModels(page, contentTag));
        }
    }

    ContentTag getContentTag() {
        return contentTag;
    }

    int getPage() {
        return page;
    }

    @Override
    public void onResponse(@NonNull Call<RetrofitParsedObject> call, @NonNull Response<RetrofitParsedObject> response) {
        if (response.isSuccessful()) {
            if (page == 1) {
                switch (getContentTag()) {
                    case TOP_RATED:
                        realmDAO.resetRating();
                        break;
                    case POPULAR:
                        realmDAO.resetPopularity();
                        break;
                    default:
                        callback.onErrorCaught(AppError.SYSTEM_ERROR);
                        return;
                }
            }
            realmDAO.insertOrUpdateParsedObjects(response, this);
        } else {
            callback.onErrorCaught(AppError.SERVER_ERROR);
        }
    }

    @Override
    public void onFailure(@NonNull Call<RetrofitParsedObject> call, @NonNull Throwable t) {
        if (t.getClass().equals(UnknownHostException.class)) {
            callback.onErrorCaught(AppError.NO_NETWORK);
        } else {
            callback.onErrorCaught(AppError.SERVER_ERROR);
        }
    }

    @Override
    public void onTransactionDone() {
        getDataFromLocalStorage();
    }

    public HomeRepositoryCallback getCallback() {
        return callback;
    }
}
