package mdd.endava.trakt.home.repository;


import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.database.dao.IRealmDAO;
import mdd.endava.trakt.network.MovieDBAPI;
import mdd.endava.trakt.network.models.movies.ParsedMovieList;

public class HomeMovieRepository extends HomeAbstractRepository<ParsedMovieList> {

    private MovieDBAPI api;

    public HomeMovieRepository(IRealmDAO realmDAO, ApplicationPreferences preferences, MovieDBAPI api) {
        super(realmDAO, preferences);
        this.api = api;
    }

    @Override
    void getDataFromWeb() {
        if (getContentTag() != null) {
            api.getMovieList(getContentTag().getApiKey(), String.valueOf(getPage()))
                    .enqueue(this);
        } else {
            getCallback().onErrorCaught(AppError.SYSTEM_ERROR);
        }
    }
}
