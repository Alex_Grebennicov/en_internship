package mdd.endava.trakt.home.repository;

import android.support.annotation.NonNull;

import java.util.List;

import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.AppError;

public interface HomeRepositoryCallback {
    void onErrorCaught(AppError error);

    void onListReceived(int page, @NonNull List<IModel> list);
}
