package mdd.endava.trakt.home.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.App;
import mdd.endava.trakt.ApplicationPreferences;
import mdd.endava.trakt.BaseActivity;
import mdd.endava.trakt.BaseFragment;
import mdd.endava.trakt.R;
import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.home.adapter.HomeAdapter;
import mdd.endava.trakt.home.adapter.HomeRecyclerAdapter;
import mdd.endava.trakt.home.dagger.DaggerHomeComponent;
import mdd.endava.trakt.home.presenter.HomePresenter;

public class HomeFragment extends BaseFragment implements HomeView, SwipeRefreshLayout.OnRefreshListener {

    private HomePresenter presenter;
    private HomeAdapter adapter;
    private ApplicationPreferences preferences;

    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private Snackbar progressSnackBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = DaggerHomeComponent.builder()
                .appComponent(App.getComponent())
                .build()
                .getPresenter();
        preferences = App.getComponent().getPreferences();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).setToolBar(getString(preferences.getCurrentVideoContentTag().getTitle()));
        }
        findAndSetupViews();
    }

    @Override
    public void onStart() {
        super.onStart();
        swipeRefreshLayout.setRefreshing(true);
        presenter.onViewStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        hideAllTheProgress();
        presenter.onViewStop();
    }

    @Override
    public void onRefresh() {
        presenter.onViewRefresh();
    }

    private void findAndSetupViews() {
        if (getView() == null) {
            return;
        }
        swipeRefreshLayout = getView().findViewById(R.id.swipeRefreshLayout);
        recyclerView = getView().findViewById(R.id.recyclerView);
        progressSnackBar = Snackbar.make(getView(), getString(R.string.progress_please_wait), Snackbar.LENGTH_INDEFINITE);
        setupProgressSnackBar();
        setupRecyclerAdapter();
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void setupRecyclerAdapter() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == linearLayoutManager.getItemCount() - 1 &&
                        !swipeRefreshLayout.isRefreshing() &&
                        !progressSnackBar.isShown()) {
                    presenter.onViewScrollEnd(adapter.getDataSize());
                }
            }
        });
        HomeRecyclerAdapter recyclerAdapter = new HomeRecyclerAdapter(new ArrayList<IModel>());
        adapter = recyclerAdapter;
        recyclerView.setAdapter(recyclerAdapter);
    }

    private void setupProgressSnackBar() {
        ViewGroup contentLayout = (ViewGroup) progressSnackBar.getView().findViewById(android.support.design.R.id.snackbar_text).getParent();
        ProgressBar progressBar = new ProgressBar(getActivity());
        progressBar.setScaleX(getResources().getInteger(R.integer.progress_bar_scale_factor_percent) / 100f);
        progressBar.setScaleY(getResources().getInteger(R.integer.progress_bar_scale_factor_percent) / 100f);
        contentLayout.addView(progressBar, 0);
    }

    private void hideAllTheProgress() {
        swipeRefreshLayout.setRefreshing(false);
        progressSnackBar.dismiss();
    }

    @Override
    public void showProgress() {
        progressSnackBar.show();
    }

    @Override
    public void showError(AppError error) {
        hideAllTheProgress();
        if (getView() != null) {
            Snackbar.make(getView(), getString(error.errorMessageResID()), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void refreshDataList(List<IModel> list) {
        hideAllTheProgress();
        if (adapter != null) {
            adapter.refreshDataList(list);
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void addToDataList(List<IModel> list) {
        hideAllTheProgress();
        if (adapter != null) {
            adapter.addToDataList(list);
            recyclerView.smoothScrollBy(0, (int) getResources().getDimension(R.dimen.common_16dp));
        }
    }
}
