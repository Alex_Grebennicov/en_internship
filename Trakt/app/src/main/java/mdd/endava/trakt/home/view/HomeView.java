package mdd.endava.trakt.home.view;

import java.util.List;

import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.AppError;

public interface HomeView {
    void showProgress();

    void showError(AppError error);

    void refreshDataList(List<IModel> list);

    void addToDataList(List<IModel> list);
}
