package mdd.endava.trakt.home.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import mdd.endava.trakt.common.IModel;

public abstract class AbstractViewHolder extends RecyclerView.ViewHolder {
    AbstractViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindData(IModel data);
}
