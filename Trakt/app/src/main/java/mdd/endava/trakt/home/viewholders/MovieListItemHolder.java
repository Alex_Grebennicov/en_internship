package mdd.endava.trakt.home.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import mdd.endava.trakt.R;
import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.database.response.MovieDBResponse;
import mdd.endava.trakt.moviedetails.MovieDetailsActivity;
import mdd.endava.trakt.network.MovieDBAPI;

public class MovieListItemHolder extends AbstractViewHolder implements Callback {

    private ImageView imageView;
    private TextView title, releaseDate, rating, adultLabel;

    public MovieListItemHolder(View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.thumbImage);
        title = itemView.findViewById(R.id.title);
        releaseDate = itemView.findViewById(R.id.release_date);
        rating = itemView.findViewById(R.id.rating);
        adultLabel = itemView.findViewById(R.id.adult);
    }

    @Override
    public void bindData(final IModel data) {
        final MovieDBResponse movie = (MovieDBResponse) data.getObject();
        if (movie == null) {
            return;
        }

        title.setText(movie.getTitle());
        releaseDate.setText(movie.getReleaseDate());
        if (movie.getVoteAverage() != 0f) {
            rating.setText(String.valueOf(movie.getVoteAverage()));
        } else {
            rating.setVisibility(View.INVISIBLE);
        }
        adultLabel.setVisibility(movie.isAdult() ? View.VISIBLE : View.GONE);
        Picasso.with(itemView.getContext())
                .load(MovieDBAPI.THUMB_IMAGE_LINK_PREFIX + movie.getThumbImagePath())
                .noPlaceholder()
                .error(R.drawable.ic_error_outline_text_secondary)
                .into(imageView, this);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemView.getContext().startActivity(
                        MovieDetailsActivity.newIntent(itemView.getContext(), movie.getId(), movie.contentType()));

            }
        });
    }

    @Override
    public void onSuccess() {
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    @Override
    public void onError() {
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }
}
