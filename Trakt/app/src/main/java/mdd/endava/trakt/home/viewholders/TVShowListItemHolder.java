package mdd.endava.trakt.home.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import mdd.endava.trakt.R;
import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.database.response.TVShowDBResponse;
import mdd.endava.trakt.moviedetails.MovieDetailsActivity;
import mdd.endava.trakt.network.MovieDBAPI;


public class TVShowListItemHolder extends AbstractViewHolder implements Callback {

    private ImageView thumbImage;
    private TextView title, firstAirDate, rating;

    public TVShowListItemHolder(View itemView) {
        super(itemView);
        thumbImage = itemView.findViewById(R.id.thumbImage);
        title = itemView.findViewById(R.id.title);
        firstAirDate = itemView.findViewById(R.id.first_air_date);
        rating = itemView.findViewById(R.id.rating);
    }

    @Override
    public void bindData(IModel data) {
        final TVShowDBResponse tvShow = (TVShowDBResponse) data.getObject();
        if (tvShow == null) {
            return;
        }
        title.setText(tvShow.getTitle());
        firstAirDate.setText(tvShow.getFirstAirDate());
        if (tvShow.getVoteAverage() != 0f) {
            rating.setText(String.valueOf(tvShow.getVoteAverage()));
        } else {
            rating.setVisibility(View.INVISIBLE);
        }
        Picasso.with(itemView.getContext())
                .load(MovieDBAPI.THUMB_IMAGE_LINK_PREFIX + tvShow.getThumbImagePath())
                .noPlaceholder()
                .error(R.drawable.ic_error_outline_text_secondary)
                .into(thumbImage, this);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemView.getContext().startActivity(
                        MovieDetailsActivity.newIntent(itemView.getContext(), tvShow.getId(), tvShow.contentType()));
            }
        });
    }

    @Override
    public void onSuccess() {
        thumbImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    @Override
    public void onError() {
        thumbImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
    }
}
