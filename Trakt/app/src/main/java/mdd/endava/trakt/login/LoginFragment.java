package mdd.endava.trakt.login;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.jakewharton.rxbinding2.widget.RxTextView;

import mdd.endava.trakt.App;
import mdd.endava.trakt.BaseFragment;
import mdd.endava.trakt.R;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.home.HomeActivity;
import mdd.endava.trakt.login.dagger.DaggerLoginComponent;
import mdd.endava.trakt.login.dagger.LoginComponent;
import mdd.endava.trakt.login.validator.ValidatorResponse;
import mdd.endava.trakt.ui.EasterEgg;

public class LoginFragment extends BaseFragment implements LoginView {

    private LoginComponent component;

    private LoginPresenter presenter;

    private ViewSwitcher viewSwitcher;
    private Button signInButton;
    private ProgressBar progressBar;
    private TextInputEditText usernameInputEditText;
    private TextInputEditText passwordInputEditText;
    private SignInTextChangeObserver usernameInputObserver;
    private SignInTextChangeObserver passwordInputObserver;

    private SecuredPreferences securedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        securedPreferences = App.getComponent().getSecuredPreferences();
        component = DaggerLoginComponent.builder().appComponent(App.getComponent()).build();
        presenter = component.getPresenter();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        usernameInputObserver = new SignInTextChangeObserver(getResources(),
                component.getUsernameValidator(),
                error -> usernameInputEditText.setError(error));
        passwordInputObserver = new SignInTextChangeObserver(getResources(),
                component.getPasswordValidator(),
                error -> passwordInputEditText.setError(error));

        RxTextView.textChanges(usernameInputEditText).subscribe(usernameInputObserver);
        RxTextView.textChanges(passwordInputEditText).subscribe(passwordInputObserver);

        presenter.onViewStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.onViewStop();
        usernameInputObserver.dispose();
        passwordInputObserver.dispose();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findAndSetupViews();
    }

    private void findAndSetupViews() {
        if (getView() == null) {
            return;
        }
        getView().findViewById(R.id.logoImage).setOnClickListener(view ->
                Toast.makeText(getActivity(), EasterEgg.getRandomSmile(), Toast.LENGTH_SHORT).show());

        usernameInputEditText = getView().findViewById(R.id.usernameTextInputEditText);

        passwordInputEditText = getView().findViewById(R.id.passwordTextInputEditText);

        CheckBox rememberCredentialsCheckBox = getView().findViewById(R.id.rememberCredentials);
        rememberCredentialsCheckBox.setChecked(securedPreferences.rememberCredentials());
        rememberCredentialsCheckBox.setOnCheckedChangeListener(
                (compoundButton, checked) -> presenter.onRememberCredentialsChecked(checked));

        viewSwitcher = getView().findViewById(R.id.viewSwitcher);
        progressBar = getView().findViewById(R.id.progressBar);
        signInButton = getView().findViewById(R.id.buttonSignIn);
        signInButton.setOnClickListener(view -> {
            if (usernameInputEditText.getError() == null && passwordInputEditText.getError() == null) {
                presenter.onLoginButtonPressed(
                        usernameInputEditText.getText().toString(),
                        passwordInputEditText.getText().toString());
            }
        });

        TextView registerButton = getView().findViewById(R.id.buttonRegister);
        registerButton.setPaintFlags(registerButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        registerButton.setOnClickListener(view ->
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.register_link)))));
    }

    @Override
    public void showError(AppError error) {
        viewSwitcher.setDisplayedChild(viewSwitcher.indexOfChild(signInButton));
        if (getView() != null) {
            Snackbar.make(getView(), getString(error.errorMessageResID()), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showError(ValidatorResponse error) {
        viewSwitcher.setDisplayedChild(viewSwitcher.indexOfChild(signInButton));
        if (getView() != null) {
            Snackbar.make(getView(), getString(error.getStringResID(), error.getArguments()), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showProgress() {
        viewSwitcher.setDisplayedChild(viewSwitcher.indexOfChild(progressBar));
    }

    @Override
    public void startNextActivity() {
        startActivity(new Intent(getActivity(), HomeActivity.class));
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void setUsername(String username) {
        usernameInputEditText.setText(username);
    }

    @Override
    public void setPassword(String password) {
        passwordInputEditText.setText(password);
    }
}
