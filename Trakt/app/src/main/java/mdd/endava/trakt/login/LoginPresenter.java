package mdd.endava.trakt.login;

public interface LoginPresenter {
    void onViewStart(LoginView view);

    void onViewStop();

    void onLoginButtonPressed(String username, String password);

    void onRememberCredentialsChecked(boolean checked);
}
