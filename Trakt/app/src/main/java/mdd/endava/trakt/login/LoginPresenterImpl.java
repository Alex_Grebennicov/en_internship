package mdd.endava.trakt.login;

import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.login.LoginNetwork;
import mdd.endava.trakt.network.login.LoginNetworkCallback;

public class LoginPresenterImpl implements LoginPresenter, LoginNetworkCallback {

    private LoginView view = null;

    private Device device;
    private SecuredPreferences preferences;
    private LoginNetwork loginNetwork;

    public LoginPresenterImpl(Device device, SecuredPreferences preferences, LoginNetwork loginNetwork) {
        this.device = device;
        this.preferences = preferences;
        this.loginNetwork = loginNetwork;
    }

    @Override
    public void onViewStart(LoginView view) {
        this.view = view;
        if (preferences.getSessionId() != null) {
            view.startNextActivity();
        } else if (preferences.rememberCredentials()) {
            view.setPassword(preferences.getPassword());
            view.setUsername(preferences.getUsername());
        }
    }

    @Override
    public void onViewStop() {
        this.view = null;
    }

    @Override
    public void onLoginButtonPressed(String username, String password) {
        username = username.trim();

        boolean shouldRememberCredentials = preferences.rememberCredentials();
        preferences.setUsername(shouldRememberCredentials ? username : null);
        preferences.setPassword(shouldRememberCredentials ? password : null);

        if (device.isOnline()) {
            loginNetwork.trySignIn(username, password, this);
            if (view != null) {
                view.showProgress();
            }
        } else if (view != null) {
            view.showError(AppError.NO_NETWORK);
        }
    }

    @Override
    public void onRememberCredentialsChecked(boolean checked) {
        preferences.setRememberCredentials(checked);
    }

    @Override
    public void onErrorCaught(AppError error) {
        if (view != null) {
            view.showError(error);
        }
    }

    @Override
    public void onLoginSucceed() {
        if (view != null) {
            view.startNextActivity();
        }
    }
}
