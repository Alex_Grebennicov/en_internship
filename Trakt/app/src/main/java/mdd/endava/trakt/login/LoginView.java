package mdd.endava.trakt.login;

import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.login.validator.ValidatorResponse;

public interface LoginView {
    void showError(AppError error);

    void showError(ValidatorResponse error);

    void showProgress();

    void startNextActivity();

    void setUsername(String username);

    void setPassword(String password);

    interface ErrorSetter {
        void setFieldError(String error);
    }
}
