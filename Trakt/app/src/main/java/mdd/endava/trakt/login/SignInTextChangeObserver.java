package mdd.endava.trakt.login;

import android.content.res.Resources;
import android.support.annotation.NonNull;

import io.reactivex.observers.DisposableObserver;
import mdd.endava.trakt.login.validator.FieldValidator;
import mdd.endava.trakt.login.validator.ValidatorResponse;

public class SignInTextChangeObserver extends DisposableObserver<CharSequence> {
    final private Resources resources;
    final private LoginView.ErrorSetter errorSetter;
    final private FieldValidator fieldValidator;

    public SignInTextChangeObserver(@NonNull Resources resources, @NonNull FieldValidator fieldValidator, @NonNull LoginView.ErrorSetter errorSetter) {
        this.resources = resources;
        this.fieldValidator = fieldValidator;
        this.errorSetter = errorSetter;
    }

    @Override
    public final void onNext(CharSequence charSequence) {
        validate(charSequence.toString().trim());
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onComplete() {

    }

    private void validate(String input) {
        ValidatorResponse response = fieldValidator.validate(input);
        errorSetter.setFieldError(response == null ? null :
                resources.getString(response.getStringResID(), response.getArguments()));
    }
}
