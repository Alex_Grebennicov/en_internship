package mdd.endava.trakt.login.dagger;

import dagger.Component;
import mdd.endava.trakt.dagger.AppComponent;
import mdd.endava.trakt.login.dagger.module.LoginModule;
import mdd.endava.trakt.login.dagger.module.LoginValidatorModule;
import mdd.endava.trakt.login.dagger.qualifier.LoginValidatorPassword;
import mdd.endava.trakt.login.dagger.qualifier.LoginValidatorUsername;
import mdd.endava.trakt.login.LoginPresenter;
import mdd.endava.trakt.login.validator.FieldValidator;

@LoginScope
@Component(modules = {LoginModule.class, LoginValidatorModule.class}, dependencies = AppComponent.class)
public interface LoginComponent {
    LoginPresenter getPresenter();

    @LoginValidatorUsername
    FieldValidator getUsernameValidator();

    @LoginValidatorPassword
    FieldValidator getPasswordValidator();
}
