package mdd.endava.trakt.login.dagger.module;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.login.dagger.LoginScope;
import mdd.endava.trakt.network.login.LoginNetwork;
import mdd.endava.trakt.network.login.LoginNetworkImpl;
import mdd.endava.trakt.login.LoginPresenter;
import mdd.endava.trakt.login.LoginPresenterImpl;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.MovieDBAPI;

@Module
public class LoginModule {
    @LoginScope
    @Provides
    LoginPresenter presenter(Device device, SecuredPreferences preferences, LoginNetwork loginNetwork) {
        return new LoginPresenterImpl(device, preferences, loginNetwork);
    }

    @LoginScope
    @Provides
    LoginNetwork loginNetwork(SecuredPreferences preferences, MovieDBAPI api) {
        return new LoginNetworkImpl(api, preferences);
    }
}
