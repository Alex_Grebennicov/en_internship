package mdd.endava.trakt.login.dagger.module;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.login.dagger.LoginScope;
import mdd.endava.trakt.login.dagger.qualifier.LoginValidatorPassword;
import mdd.endava.trakt.login.dagger.qualifier.LoginValidatorUsername;
import mdd.endava.trakt.login.validator.FieldValidator;

@Module
public class LoginValidatorModule {

    @LoginScope
    @Provides
    @LoginValidatorUsername
    FieldValidator usernameValidator() {
        return new FieldValidator();
    }

    @LoginScope
    @Provides
    @LoginValidatorPassword
    FieldValidator passwordValidator() {
        return new FieldValidator(FieldValidator.DEFAULT_MIN_PASSWORD_LENGTH);
    }
}
