package mdd.endava.trakt.login.dagger.qualifier;

import javax.inject.Qualifier;

@Qualifier
public @interface LoginValidatorUsername {
}
