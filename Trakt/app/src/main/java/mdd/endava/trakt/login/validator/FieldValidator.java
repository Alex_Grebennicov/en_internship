package mdd.endava.trakt.login.validator;

import mdd.endava.trakt.R;

public class FieldValidator {

    public final static int DEFAULT_MIN_LENGTH = 0;
    public final static int DEFAULT_MIN_PASSWORD_LENGTH = 4;

    final private int minLength;

    public FieldValidator() {
        this.minLength = DEFAULT_MIN_LENGTH;
    }

    public FieldValidator(int minLength) {
        this.minLength = minLength;
    }

    public ValidatorResponse validate(String input) {
        ValidatorResponse response = new ValidatorResponse();
        if (input.isEmpty()) {
            response.setStringResID(R.string.input_error_required_field);
            return response;
        }
        if (input.length() < minLength) {
            response.setStringResID(R.string.input_error_min_length);
            response.setArguments(new Object[]{minLength});
            return response;
        }
        return null;
    }
}
