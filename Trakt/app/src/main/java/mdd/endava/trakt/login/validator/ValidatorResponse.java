package mdd.endava.trakt.login.validator;

public class ValidatorResponse {

    private int stringResID;
    private Object[] arguments;

    public int getStringResID() {
        return stringResID;
    }

    public void setStringResID(int stringResID) {
        this.stringResID = stringResID;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public void setArguments(Object[] arguments) {
        this.arguments = arguments;
    }
}
