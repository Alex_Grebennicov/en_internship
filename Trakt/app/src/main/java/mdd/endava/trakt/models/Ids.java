package mdd.endava.trakt.models;

/**
 * Created by cmardari on 09-Oct-17.
 */

public class Ids {

    private Integer trakt;
    private String slug;
    private String imdb;
    private long tmdb;

    public Integer getTrakt() {
        return trakt;
    }

    public void setTrakt(Integer trakt) {
        this.trakt = trakt;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public long getTmdb() {
        return tmdb;
    }

    public void setTmdb(long tmdb) {
        this.tmdb = tmdb;
    }

}