package mdd.endava.trakt.models;

public class MovieExtendedResponse {

    private String type;
    private Integer score;
    private MovieExtended movie;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public MovieExtended getMovie() {
        return movie;
    }

    public void setMovie(MovieExtended movie) {
        this.movie = movie;
    }
}