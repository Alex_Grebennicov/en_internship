package mdd.endava.trakt.models;

public class MovieFeaturesInfo implements MovieInfo {
    private String cellTitle;
    private String cellContent;

    public MovieFeaturesInfo(String cellTitle, String cellContent) {
        this.cellTitle = cellTitle;
        this.cellContent = cellContent;
    }

    @Override
    public int getType() {
        return DETAILS_INFO_TYPE;
    }

    public String getCellTitle() {
        return cellTitle;
    }

    public void setCellTitle(String cellTitle) {
        this.cellTitle = cellTitle;
    }

    public String getCellContent() {
        return cellContent;
    }

    public void setCellContent(String cellContent) {
        this.cellContent = cellContent;
    }
}
