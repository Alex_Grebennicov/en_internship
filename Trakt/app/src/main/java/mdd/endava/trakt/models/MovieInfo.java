package mdd.endava.trakt.models;

public interface MovieInfo {
    int CAST_INFO_TYPE = 1;
    int DETAILS_INFO_TYPE = 2;
    int OVERVIEW_INFO_TYPE = 3;

    int getType();
}
