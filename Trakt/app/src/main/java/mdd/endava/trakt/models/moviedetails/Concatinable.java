package mdd.endava.trakt.models.moviedetails;

/**
 * Created by cmardari on 25-Oct-17.
 */

public interface Concatinable {
    String getName();
}
