
package mdd.endava.trakt.models.moviedetails;

import io.realm.RealmObject;

public class  Genre extends RealmObject implements Concatinable{

    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
