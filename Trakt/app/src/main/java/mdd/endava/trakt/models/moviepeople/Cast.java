
package mdd.endava.trakt.models.moviepeople;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import mdd.endava.trakt.models.MovieInfo;

public class Cast extends RealmObject implements MovieInfo {

    @SerializedName("cast_id")
    @Expose
    private Integer castId;
    private String character;
    @SerializedName("credit_id")
    @Expose
    private String creditId;
    private Integer gender;
    private Integer id;
    private String name;
    private Integer order;
    @SerializedName("profile_path")
    @Expose
    private String profilePath = null;

    public Integer getCastId() {
        return castId;
    }

    public void setCastId(Integer castId) {
        this.castId = castId;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    @Override
    public int getType() {
        return CAST_INFO_TYPE;
    }
}
