package mdd.endava.trakt.moviedetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import mdd.endava.trakt.BaseActivity;
import mdd.endava.trakt.R;
import mdd.endava.trakt.common.enums.ContentType;

public class MovieDetailsActivity extends BaseActivity {
    public static final String ID_KEY = "_id";
    public static final String TYPE_KEY = "type";
    long selectedMovieId;
    ContentType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedMovieId = getIntent().getExtras().getLong(ID_KEY, -1);
        type = (ContentType) getIntent().getExtras().getSerializable(TYPE_KEY);

        FragmentManager fragmentManager = getSupportFragmentManager();
        MovieDetailsFragment movieDetailsFragment = (MovieDetailsFragment) fragmentManager.findFragmentByTag(MovieDetailsFragment.FRAGMENT_DETAILS_TAG);
        if (movieDetailsFragment == null) {
            movieDetailsFragment = MovieDetailsFragment.newInstance(selectedMovieId, type);
            fragmentManager.beginTransaction().replace(R.id.movie_details_container, movieDetailsFragment, MovieDetailsFragment.FRAGMENT_DETAILS_TAG).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tool_bar_menu_movie_details, menu);
        return true;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_movie_details;
    }

    public static Intent newIntent(Context context, long selectedMovieId, ContentType type) {
        Intent intent = new Intent(context, MovieDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong(ID_KEY, selectedMovieId);
        bundle.putSerializable(TYPE_KEY, type);
        intent.putExtras(bundle);
        return intent;
    }
}
