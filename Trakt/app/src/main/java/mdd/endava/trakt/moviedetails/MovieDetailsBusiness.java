package mdd.endava.trakt.moviedetails;

import mdd.endava.trakt.moviedetails.data.DataSource;
import mdd.endava.trakt.moviedetails.data.LocalDataSource;
import mdd.endava.trakt.moviedetails.data.RemoteDataSource;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;

public class MovieDetailsBusiness {
    private DataSource localDataSource;
    private DataSource remoteDataSource;

    public MovieDetailsBusiness(LocalDataSource local, RemoteDataSource remote) {
        this.localDataSource = local;
        this.remoteDataSource = remote;
    }

    public DataViewModel getMovieData(long id) {
        return localDataSource.getMovieData(id);
    }

    public DataViewModel getNewMovieData(long id) {
        return remoteDataSource.getMovieData(id);
    }

    public DataViewModel getTvshowData(long id) {
        return localDataSource.getTvShowData(id);
    }

    public DataViewModel getNewTvshowData(long id) {
        return remoteDataSource.getTvShowData(id);
    }

    public void saveMovieData(long id, DataViewModel data) {
        localDataSource.saveOrUpdateMovieData(id, data);
    }

    public void saveTvshowData(long id, DataViewModel data) {
        localDataSource.saveOrUpdateTvshowData(id, data);
    }

    public void closeDb() {
        ((LocalDataSource) localDataSource).close();
    }
}
