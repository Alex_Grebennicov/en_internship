package mdd.endava.trakt.moviedetails;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import mdd.endava.trakt.App;
import mdd.endava.trakt.BaseActivity;
import mdd.endava.trakt.R;
import mdd.endava.trakt.common.customviews.ImageSlider;
import mdd.endava.trakt.common.customviews.RatingWidget;
import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.models.MovieFeaturesInfo;
import mdd.endava.trakt.models.MovieInfo;
import mdd.endava.trakt.moviedetails.dagger.DaggerMovieDetailsComponent;
import mdd.endava.trakt.moviedetails.dagger.MovieDetailsModule;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.MovieDataViewModel;
import mdd.endava.trakt.moviedetails.data.models.TvshowDataViewModel;
import mdd.endava.trakt.moviedetails.movieimages.ViewPagerAdapter;
import mdd.endava.trakt.moviedetails.movieinfo.MovieInfoAdapter;
import mdd.endava.trakt.moviedetails.seasons.SeasonsAdapter;

public class MovieDetailsFragment extends Fragment implements MovieDetailsView {
    public static final String FRAGMENT_DETAILS_TAG = MovieDetailsFragment.class.getSimpleName();

    @Inject
    MovieDetailsPresenter presenter;
    @Inject
    MovieInfoAdapter movieInfoAdapter;
    @Inject
    ViewPagerAdapter imagesAdapter;
    @Inject
    SeasonsAdapter seasonsAdapter;

    private long selectedId;
    private ContentType selectedType;

    private CollapsingToolbarLayout collapsingToolbar;
    private TextView yearTxtView;
    private TextView runtimeTxtView;
    private TextView overviewTxtView;
    private TextView seasonTitle;
    private TextView tagLineTextView;
    private RatingWidget ratingWidget;
    private FloatingActionButton fabTrailer;
    private Button homepageButton;
    private Button imdbButton;
    private LinearLayout movieContentLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageSlider imageSlider;

    private List<MovieInfo> arrayOfData = new ArrayList<>();

    public static MovieDetailsFragment newInstance(long selectedId, ContentType type) {
        Bundle args = new Bundle();
        args.putLong(MovieDetailsActivity.ID_KEY, selectedId);
        args.putSerializable(MovieDetailsActivity.TYPE_KEY, type);
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        if (getArguments() != null) {
            selectedId = getArguments().getLong(MovieDetailsActivity.ID_KEY);
            selectedType = (ContentType) getArguments().getSerializable(MovieDetailsActivity.TYPE_KEY);
        }

        DaggerMovieDetailsComponent.builder()
                .appComponent(App.getComponent())
                .movieDetailsModule(new MovieDetailsModule(this))
                .build()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.fragment_movie_details, container, false);

        imageSlider = parentView.findViewById(R.id.image_slider);
        swipeRefreshLayout = parentView.findViewById(R.id.swipe_movie_details);
        collapsingToolbar = parentView.findViewById(R.id.collapsing_toolbar);
        yearTxtView = parentView.findViewById(R.id.movie_year);
        runtimeTxtView = parentView.findViewById(R.id.movie_runtime);
        overviewTxtView = parentView.findViewById(R.id.movie_overview);
        tagLineTextView = parentView.findViewById(R.id.movie_tagline);
        ratingWidget = parentView.findViewById(R.id.rating_widget);
        fabTrailer = parentView.findViewById(R.id.fab_trailer);
        homepageButton = parentView.findViewById(R.id.homepage_button);
        imdbButton = parentView.findViewById(R.id.imdb_button);
        seasonTitle = parentView.findViewById(R.id.seasons_title);
        movieContentLayout = parentView.findViewById(R.id.movie_extended_info);

        if (getActivity() != null) {
            ((BaseActivity) getActivity()).setToolBar((Toolbar) parentView.findViewById(R.id.toolbar));
        }
        collapsingToolbar.setCollapsedTitleTextColor(ResourcesCompat.getColor(getResources(), R.color.colorAccentDark, null));
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ToolBarTextExpanded);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.ToolBarTextCollapsed);

        RecyclerView recyclerView = parentView.findViewById(R.id.movie_cast_list);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(movieInfoAdapter);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(linearLayoutManager);
        }
        imageSlider.setAdapter(imagesAdapter);

        RecyclerView seasonsRecyclerView = parentView.findViewById(R.id.seasons_recycler);
        LinearLayoutManager linearLayoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        seasonsRecyclerView.setNestedScrollingEnabled(false);
        seasonsRecyclerView.setAdapter(seasonsAdapter);
        seasonsRecyclerView.setLayoutManager(linearLayoutManager);
        return parentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        presenter.onStart(selectedId, selectedType);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.onStart(selectedId, selectedType);
            }
        });
    }

    @Override
    public void setProgressBar(boolean show) {
        swipeRefreshLayout.setRefreshing(show);
    }

    @Override
    public void showData(DataViewModel data) {
        imagesAdapter.clear();
        movieInfoAdapter.clear();

        if (selectedType == ContentType.MOVIE) {
            MovieDataViewModel movieData = (MovieDataViewModel) data;
            checkNotNullAndAdd(getString(R.string.released_title), movieData.getReleaseDate());
            checkNotNullAndAdd(getString(R.string.countries_title), movieData.getProdCountries());
            tagLineTextView.setText(movieData.getTagline());
            imdbButton.setOnClickListener(new OnLinkClickListener(movieData.getImdbUrl()));
        } else if (selectedType == ContentType.TV_SHOW) {
            seasonsAdapter.clear();
            seasonTitle.setVisibility(View.VISIBLE);
            TvshowDataViewModel tvshowData = (TvshowDataViewModel) data;
            checkNotNullAndAdd(getString(R.string.status_title), tvshowData.getStatus());
            checkNotNullAndAdd(getString(R.string.countries_title), tvshowData.getOriginCountries());
            checkNotNullAndAdd(getString(R.string.network_title), tvshowData.getNetworks());
            imdbButton.setVisibility(View.GONE);
            seasonsAdapter.setData(tvshowData.getSeasons());
        }

        collapsingToolbar.setTitle(data.getTitle());
        yearTxtView.setText(data.getYear());
        runtimeTxtView.setText(data.getRuntime());
        ratingWidget.setRating(data.getVoteAverage());
        if (tagLineTextView.getText().toString().isEmpty()) {
            tagLineTextView.setVisibility(View.GONE);
        }
        overviewTxtView.setText(data.getOverview());
        checkNotNullAndAdd(getString(R.string.language_title), data.getOriginalLanguage());
        checkNotNullAndAdd(getString(R.string.genre_title), data.getGenres());

        if (data.getCastList() != null) {
            arrayOfData.addAll(data.getCastList());
        }
        movieInfoAdapter.setData(arrayOfData);
        movieContentLayout.setVisibility(View.VISIBLE);

        imagesAdapter.setData(data.getBackdropList());
        if (imagesAdapter.getCount() > 1) {
            imageSlider.addDots();
        }

        fabTrailer.setOnClickListener(new OnLinkClickListener(data.getTrailerUrl()));
        homepageButton.setOnClickListener(new OnLinkClickListener(data.getHomepageUrl()));
    }

    @Override
    public void showError(int resource) {
        Toast.makeText(getContext(), getResources().getString(resource), Toast.LENGTH_SHORT).show();
    }

    void checkNotNullAndAdd(String title, String content) {
        if (content != null) {
            arrayOfData.add(new MovieFeaturesInfo(title, content));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
