package mdd.endava.trakt.moviedetails;

import mdd.endava.trakt.R;
import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.asyncupdate.LoaderData;
import mdd.endava.trakt.moviedetails.data.asyncupdate.UpdateDataCallback;
import mdd.endava.trakt.network.Device;

public class MovieDetailsPresenter implements UpdateDataCallback {
    private MovieDetailsBusiness movieDetailsBusiness;
    private MovieDetailsView view;
    private Device device;
    private LoaderData loaderData;

    private long selectedId;
    private ContentType selectedType;

    public MovieDetailsPresenter(MovieDetailsView view, MovieDetailsBusiness movieDetailsBusiness, Device device, LoaderData loaderData) {
        this.movieDetailsBusiness = movieDetailsBusiness;
        this.view = view;
        this.device = device;
        this.loaderData = loaderData;
    }

    public void onStart(final long selectedId, ContentType type) {
        this.selectedId = selectedId;
        this.selectedType = type;

        if (type == ContentType.MOVIE){
            DataViewModel data = movieDetailsBusiness.getMovieData(selectedId);
            view.showData(data);
        } else if (type == ContentType.TV_SHOW) {
            DataViewModel data = movieDetailsBusiness.getTvshowData(selectedId);
            view.showData(data);
        }

        if (device.isOnline()) {
            view.setProgressBar(true);
            loaderData.loadData(selectedId, selectedType, movieDetailsBusiness, this);
        } else {
            view.setProgressBar(false);
            view.showError(R.string.no_internet_connection);
        }
    }

    public void onDestroy() {
        view = null;
        movieDetailsBusiness.closeDb();
        movieDetailsBusiness = null;
    }

    @Override
    public void onSuccess(DataViewModel data) {
        if (view != null) {
            view.setProgressBar(false);
            view.showData(data);
        }
    }

    @Override
    public void onFailure(int error) {
        if (view != null) {
            view.showError(error);
        }
    }

    @Override
    public void onSaveData(DataViewModel newData) {
        if (movieDetailsBusiness != null) {
            if (selectedType == ContentType.MOVIE) {
                movieDetailsBusiness.saveMovieData(selectedId, newData);
            } else if (selectedType == ContentType.TV_SHOW) {
                movieDetailsBusiness.saveTvshowData(selectedId, newData);
            }
        }
    }
}
