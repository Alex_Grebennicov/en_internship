package mdd.endava.trakt.moviedetails;

import android.content.Context;

import mdd.endava.trakt.moviedetails.data.models.DataViewModel;

public interface MovieDetailsView {

    Context getContext();

    void showData(DataViewModel data);

    void showError(int resource);

    void setProgressBar(boolean show);
}
