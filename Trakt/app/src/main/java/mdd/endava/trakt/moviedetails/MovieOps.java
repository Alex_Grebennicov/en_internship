package mdd.endava.trakt.moviedetails;

import java.util.List;

import mdd.endava.trakt.models.moviedetails.Concatinable;

public class MovieOps {

    public static String concatArray(List<? extends Concatinable> list) {
        if (list != null) {
            StringBuilder countriesString = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (i != list.size() - 1 && i != 0) {
                    countriesString.append(", ");
                }
                countriesString.append(list.get(i).getName());
            }
            if (countriesString.length() != 0) {
                return new String(countriesString);
            }
        }
        return null;
    }

    public static String concatArrayOfStrings(List<String> list) {
        if (list != null) {
            StringBuilder countriesString = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (i != list.size() - 1 && i != 0) {
                    countriesString.append(", ");
                }
                countriesString.append(list.get(i));
            }
            if (countriesString.length() != 0) {
                return new String(countriesString);
            }
        }
        return null;
    }

    public static String getTimeInGoodFormat(Integer timeInt) {
        if (timeInt > 0) {
            int minutes = timeInt % 60;
            int hours = timeInt / 60;

            if (hours != 0 && minutes != 0) {
                return (hours + "h " + minutes + "m");
            } else if (minutes == 0) {
                return (hours + "h");
            }
            return (minutes + "m");
        }
        return "-";
    }

    public static String getYear(String releasedDate) {
        if (releasedDate != null) {
            return (releasedDate.isEmpty()) ? "-" : releasedDate.substring(0, 4);
        }
        return "-";
    }

}
