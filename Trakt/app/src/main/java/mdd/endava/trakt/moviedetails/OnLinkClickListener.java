package mdd.endava.trakt.moviedetails;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;

import mdd.endava.trakt.network.Device;

/**
 * Created by cmardari on 18-Oct-17.
 */

public class OnLinkClickListener implements View.OnClickListener {
    private String url;

    public OnLinkClickListener(String url) {
        this.url = url;
    }

    @Override
    public void onClick(View view) {
        Device device = new Device(view.getContext());

        if (url == null) {
            Toast.makeText(view.getContext(), "ERROR. Can't open the link.", Toast.LENGTH_LONG).show();
        } else if (url.isEmpty()) {
            Toast.makeText(view.getContext(), "ERROR. Can't open the link.", Toast.LENGTH_LONG).show();
        } else if (!device.isOnline()) {
            Toast.makeText(view.getContext(), "ERROR. No network connection.", Toast.LENGTH_LONG).show();
        } else {
            view.getContext().startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }
}