package mdd.endava.trakt.moviedetails.dagger;

import dagger.Component;
import mdd.endava.trakt.dagger.AppComponent;
import mdd.endava.trakt.moviedetails.MovieDetailsFragment;

@MovieDetailsScope
@Component(modules = MovieDetailsModule.class, dependencies = AppComponent.class)
public interface MovieDetailsComponent {
    void inject(MovieDetailsFragment movieDetailsFragment);
}

