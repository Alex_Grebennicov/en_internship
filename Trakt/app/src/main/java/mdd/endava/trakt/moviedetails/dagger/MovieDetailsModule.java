package mdd.endava.trakt.moviedetails.dagger;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import mdd.endava.trakt.App;
import mdd.endava.trakt.database.RealmManager;
import mdd.endava.trakt.moviedetails.MovieDetailsBusiness;
import mdd.endava.trakt.moviedetails.MovieDetailsPresenter;
import mdd.endava.trakt.moviedetails.MovieDetailsView;
import mdd.endava.trakt.moviedetails.data.LocalDataSource;
import mdd.endava.trakt.moviedetails.data.MovieDetailsServiceApi;
import mdd.endava.trakt.moviedetails.data.RemoteDataSource;
import mdd.endava.trakt.moviedetails.data.asyncupdate.LoaderData;
import mdd.endava.trakt.moviedetails.data.models.DataViewModelConverter;
import mdd.endava.trakt.moviedetails.movieimages.ViewPagerAdapter;
import mdd.endava.trakt.moviedetails.movieinfo.MovieInfoAdapter;
import mdd.endava.trakt.moviedetails.seasons.SeasonsAdapter;
import mdd.endava.trakt.network.Device;

@Module
public class MovieDetailsModule {

    private MovieDetailsView view;

    public MovieDetailsModule(MovieDetailsView view) {
        this.view = view;
    }

    @MovieDetailsScope
    @Provides
    public MovieDetailsPresenter providePresenter(MovieDetailsBusiness movieDetailsBusiness, Device device, LoaderData loaderData) {
        return new MovieDetailsPresenter(view, movieDetailsBusiness, device, loaderData);
    }

    @MovieDetailsScope
    @Provides
    public LoaderData provideLoaderData() {
        return new LoaderData();
    }

    @MovieDetailsScope
    @Provides
    public MovieDetailsBusiness provideMovieDetailsBusinessLogic(LocalDataSource localDataSource, RemoteDataSource remoteDataSource) {
        return new MovieDetailsBusiness(localDataSource, remoteDataSource);
    }

    @MovieDetailsScope
    @Provides
    public LocalDataSource provideLocalDataSource(RealmManager realmManager, DataViewModelConverter converter) {
        return new LocalDataSource(realmManager, converter);
    }

    @MovieDetailsScope
    @Provides
    public RemoteDataSource provideRemoteDataSource(MovieDetailsServiceApi movieDBApi, DataViewModelConverter converter) {
        return new RemoteDataSource(movieDBApi, converter);
    }

    @MovieDetailsScope
    @Provides
    public RealmManager provideRealmManager() {
        return new RealmManager();
    }

    @MovieDetailsScope
    @Provides
    public MovieDetailsServiceApi provideMovieDetailsServiceApi() {
        return new MovieDetailsServiceApi(App.getComponent().getAPI());
    }

    @MovieDetailsScope
    @Provides
    public DataViewModelConverter provideConverter() {
        return new DataViewModelConverter();
    }

    @MovieDetailsScope
    @Provides
    public MovieInfoAdapter provideMovieInfoAdapter() {
        return new MovieInfoAdapter();
    }

    @MovieDetailsScope
    @Provides
    public ViewPagerAdapter provideViewPagerAdapter(Context context) {
        return new ViewPagerAdapter(context);
    }

    @MovieDetailsScope
    @Provides
    public SeasonsAdapter provideSeasonsAdapter() {
        return new SeasonsAdapter();
    }


}


