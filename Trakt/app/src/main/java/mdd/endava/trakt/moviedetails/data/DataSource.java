package mdd.endava.trakt.moviedetails.data;

import mdd.endava.trakt.moviedetails.data.models.DataViewModel;

public interface DataSource {

    DataViewModel getMovieData(long id);

    DataViewModel getTvShowData(long id);

    void saveOrUpdateMovieData(long id, DataViewModel data);

    void saveOrUpdateTvshowData(long id, DataViewModel data);
}
