package mdd.endava.trakt.moviedetails.data;

import mdd.endava.trakt.database.RealmManager;
import mdd.endava.trakt.database.model.MovieDBModel;
import mdd.endava.trakt.database.model.MovieDetails;
import mdd.endava.trakt.database.model.TVShowDBModel;
import mdd.endava.trakt.database.model.TvshowDetails;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.DataViewModelConverter;

public class LocalDataSource implements DataSource {
    private RealmManager realmManager;
    private DataViewModelConverter converter;

    public LocalDataSource(RealmManager realmManager, DataViewModelConverter converter) {
        this.realmManager = realmManager;
        this.converter = converter;
        realmManager.open();
    }

    @Override
    public DataViewModel getMovieData(long id) {
        MovieDetails movieDetails = realmManager.createMovieDetailsDao().getById(id);
        MovieDBModel movieWithDefaultData = realmManager.createMovieDao().getById(id);
        if (movieDetails != null) {
            return converter.fromMovieAndDetails(movieWithDefaultData, movieDetails);
        } else {
            if (movieWithDefaultData != null) {
                return converter.fromMovie(movieWithDefaultData);
            }
        }
        return null;
    }

    @Override
    public DataViewModel getTvShowData(long id) {
        TvshowDetails tvshowDetails = realmManager.createTvshowDetailsDao().getById(id);
        TVShowDBModel tvshowWithDefaultData = realmManager.createTvshowDao().getById(id);
        if (tvshowDetails != null) {
            return converter.fromTvshowAndDetails(tvshowWithDefaultData, tvshowDetails);
        } else {
            if (tvshowWithDefaultData != null) {
                return converter.fromTvshow(tvshowWithDefaultData);
            }
        }
        return null;
    }

    @Override
    public void saveOrUpdateMovieData(long id, DataViewModel data) {
        realmManager.createMovieDetailsDao().save(id, data);
    }

    @Override
    public void saveOrUpdateTvshowData(long id, DataViewModel data) {
        realmManager.createTvshowDetailsDao().save(id, data);
    }

    public void close() {
        realmManager.close();
    }
}
