package mdd.endava.trakt.moviedetails.data;

import java.io.IOException;
import java.util.List;

import mdd.endava.trakt.models.moviedetails.MovieDetailed;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.movieimages.Images;
import mdd.endava.trakt.models.moviepeople.Cast;
import mdd.endava.trakt.models.moviepeople.Credits;
import mdd.endava.trakt.models.tvdetails.TvDetailed;
import mdd.endava.trakt.models.video.Video;
import mdd.endava.trakt.network.MovieDBAPI;
import retrofit2.Response;

import static mdd.endava.trakt.network.MovieDBAPI.API_KEY;

public class MovieDetailsServiceApi {
    public static final int MAX_NUM_OF_CAST = 10;
    public static final int MAX_NUM_OF_BACKDROPS = 20;

    private MovieDBAPI movieDBAPI;

    public MovieDetailsServiceApi(MovieDBAPI movieDBAPI) {
        this.movieDBAPI = movieDBAPI;
    }

    public TvDetailed getTvshowInfo(long selectedId) throws IOException {
        Response<TvDetailed> responseTvshoeInfo = movieDBAPI.getTvShowDetails(String.valueOf(selectedId), API_KEY).execute();
        if (responseTvshoeInfo.isSuccessful()) {
            return responseTvshoeInfo.body();
        }
        return null;
    }

    public MovieDetailed getMovieInfo(long selectedId) throws IOException {
        Response<MovieDetailed> responseMovieInfo = movieDBAPI.
                getMovieDetails(String.valueOf(selectedId),
                        API_KEY).execute();

        if (responseMovieInfo.isSuccessful()) {
            return responseMovieInfo.body();
        }
        return null;
    }


    public List<Backdrop> getMovieImages(long selectedId) throws IOException {
        Response<Images> responseMovieImages = movieDBAPI.getMovieImages(String.valueOf(selectedId), API_KEY).execute();
        if (responseMovieImages.isSuccessful()) {
            List<Backdrop> backdrops = responseMovieImages.body().getBackdrops();
            if (backdrops == null) {
                return null;
            }
            if (!backdrops.isEmpty()) {
                if (backdrops.size() > MAX_NUM_OF_BACKDROPS) {
                    return backdrops.subList(0, MAX_NUM_OF_BACKDROPS);
                } else {
                    return backdrops;
                }
            }
        }
        return null;
    }

    public List<Backdrop> getTvshowImages(long selectedId) throws IOException {
        Response<Images> responseTvshowImages = movieDBAPI.getTvShowImages(String.valueOf(selectedId), API_KEY).execute();
        if (responseTvshowImages.isSuccessful()) {
            List<Backdrop> backdrops = responseTvshowImages.body().getBackdrops();
            if (backdrops == null) {
                return null;
            }

            if (!backdrops.isEmpty()) {
                if (backdrops.size() > MAX_NUM_OF_BACKDROPS) {
                    return backdrops.subList(0, MAX_NUM_OF_BACKDROPS);
                } else {
                    return backdrops;
                }
            }
        }
        return null;
    }

    public String getMovieYoutubeKey(long selectedId) throws IOException {
        Response<Video> responseVideo = movieDBAPI.getMovieVideos(String.valueOf(selectedId), API_KEY).execute();
        if (responseVideo.isSuccessful()) {
            Video video = responseVideo.body();
            if (video != null) {
                if (!video.getResults().isEmpty()) {
                    return video.getResults().get(0).getKey();
                }
            }
        }
        return null;
    }

    public String getTvshowYoutubeKey(long selectedId) throws IOException {
        Response<Video> responseVideo = movieDBAPI.getTvShowVideo(String.valueOf(selectedId), API_KEY).execute();
        if (responseVideo.isSuccessful()) {
            Video video = responseVideo.body();
            if (video != null) {
                if (!video.getResults().isEmpty()) {
                    return video.getResults().get(0).getKey();
                }
            }
        }
        return null;
    }

    public List<Cast> getMovieCredits(long selectedId) throws IOException {
        Response<Credits> responseMoviePeople = movieDBAPI.getMovieCredits(String.valueOf(selectedId), API_KEY).execute();
        if (responseMoviePeople.isSuccessful()) {
            List<Cast> castList = responseMoviePeople.body().getCast();
            if (castList == null) {
                return null;
            }

            if (castList.size() > MAX_NUM_OF_CAST) {
                return castList.subList(0, MAX_NUM_OF_CAST);
            } else {
                return castList;
            }
        }
        return null;
    }

    public List<Cast> getTvshowCredits(long selectedId) throws IOException {
        Response<Credits> reponseTvshowPeople = movieDBAPI.getTvShowCredits(String.valueOf(selectedId), API_KEY).execute();
        if (reponseTvshowPeople.isSuccessful()) {
            List<Cast> castList = reponseTvshowPeople.body().getCast();
            if (castList == null) {
                return null;
            }

            if (castList.size() > MAX_NUM_OF_CAST) {
                return castList.subList(0, MAX_NUM_OF_CAST);
            } else {
                return castList;
            }
        }
        return null;
    }

}
