package mdd.endava.trakt.moviedetails.data;

import java.io.IOException;
import java.util.List;

import mdd.endava.trakt.models.moviedetails.MovieDetailed;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.moviepeople.Cast;
import mdd.endava.trakt.models.tvdetails.TvDetailed;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.DataViewModelConverter;

public class RemoteDataSource implements DataSource {


    private MovieDetailsServiceApi movieDetailsServiceApi;
    private DataViewModelConverter converter;

    public RemoteDataSource(MovieDetailsServiceApi movieDBAPI, DataViewModelConverter dataConverter) {
        this.movieDetailsServiceApi = movieDBAPI;
        converter = dataConverter;
    }

    @Override
    public DataViewModel getMovieData(long selectedId) {
        try {
            MovieDetailed movieDetailed = movieDetailsServiceApi.getMovieInfo(selectedId);
            String youtubeKey = movieDetailsServiceApi.getMovieYoutubeKey(selectedId);
            List<Backdrop> backdropList = movieDetailsServiceApi.getMovieImages(selectedId);
            List<Cast> castList = movieDetailsServiceApi.getMovieCredits(selectedId);
            return converter.fromMovieRequests(movieDetailed, youtubeKey, castList, backdropList);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public DataViewModel getTvShowData(long selectedId) {
        try {
            TvDetailed tvShowDetailed = movieDetailsServiceApi.getTvshowInfo(selectedId);
            String youtubeKey = movieDetailsServiceApi.getTvshowYoutubeKey(selectedId);
            List<Backdrop> backdropList = movieDetailsServiceApi.getTvshowImages(selectedId);
            List<Cast> castList = movieDetailsServiceApi.getTvshowCredits(selectedId);
            return converter.fromTvShowRequests(tvShowDetailed, youtubeKey, castList, backdropList);
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void saveOrUpdateMovieData(long id,  DataViewModel data) {
    }

    @Override
    public void saveOrUpdateTvshowData(long id,  DataViewModel data) {
    }
}
