package mdd.endava.trakt.moviedetails.data.asyncupdate;

import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.moviedetails.MovieDetailsBusiness;

public class LoaderData {
    UpdateDataAsync updateDataAsync;

    public void loadData(long selectedId, ContentType selectedType, MovieDetailsBusiness movieDetailsBusiness, UpdateDataCallback callback) {
        updateDataAsync = new UpdateDataAsync(selectedId, selectedType, movieDetailsBusiness, callback);
        updateDataAsync.execute();
    }
}
