package mdd.endava.trakt.moviedetails.data.asyncupdate;

import android.os.AsyncTask;

import mdd.endava.trakt.R;
import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.moviedetails.MovieDetailsBusiness;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;

public class UpdateDataAsync extends AsyncTask<Void, Void, DataViewModel> {
    private UpdateDataCallback callback;
    private MovieDetailsBusiness movieDetailsBusiness;
    private long selectedId;
    private ContentType selectedType;

    public UpdateDataAsync(long selectedId, ContentType selectedType, MovieDetailsBusiness movieDetailsBusiness, UpdateDataCallback callback) {
        this.callback = callback;
        this.selectedId = selectedId;
        this.selectedType = selectedType;
        this.movieDetailsBusiness = movieDetailsBusiness;
    }

    @Override
    protected DataViewModel doInBackground(Void... voids) {
        DataViewModel data = null;
        if (selectedType == ContentType.MOVIE) {
            data = movieDetailsBusiness.getNewMovieData(selectedId);
        } else if (selectedType == ContentType.TV_SHOW) {
            data = movieDetailsBusiness.getNewTvshowData(selectedId);
        }
        return data;
    }

    @Override
    public void onPostExecute(DataViewModel data) {
        if (data == null) {
            callback.onFailure(R.string.no_response);
        } else {
            callback.onSaveData(data);
            callback.onSuccess(data);
        }
    }
}
