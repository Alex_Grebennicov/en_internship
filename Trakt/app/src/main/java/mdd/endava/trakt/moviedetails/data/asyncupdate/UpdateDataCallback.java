package mdd.endava.trakt.moviedetails.data.asyncupdate;

import mdd.endava.trakt.moviedetails.data.models.DataViewModel;

public interface UpdateDataCallback {


    void onSuccess(DataViewModel data);

    void onFailure(int no_response);

    void onSaveData(DataViewModel data);
}
