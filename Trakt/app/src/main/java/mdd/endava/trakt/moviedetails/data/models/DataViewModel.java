package mdd.endava.trakt.moviedetails.data.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mdd.endava.trakt.models.moviedetails.Genre;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.moviepeople.Cast;

public class DataViewModel {
    public static final String YOUTUBE_URL = "https://www.youtube.com/watch?v=";

    private String title;
    private String year;
    private String overview;
    private String backdropImage;
    private String originalLanguage;
    private float voteAverage;
    private String homepageUrl;
    private String trailerUrl;
    private String youtubeId;
    private int runtimeMin;
    private String runtime;
    private List<Genre> genresList = Collections.emptyList();
    private String genres;
    private List<Cast> castList = new ArrayList<>();
    private List<Backdrop> backdropList = new ArrayList<>();

    public List<Genre> getGenresList() {
        return genresList;
    }

    public void setGenresList(List<Genre> genresList) {
        this.genresList = genresList;
    }

    public String getYoutubeId() {
        return youtubeId;
    }

    public void setYoutubeId(String youtubeId) {
            this.youtubeId = youtubeId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getBackdropImage() {
        return backdropImage;
    }

    public void setBackdropImage(String backdropImage) {
        this.backdropImage = backdropImage;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getHomepageUrl() {
        return homepageUrl;
    }

    public void setHomepageUrl(String homepageUrl) {
        this.homepageUrl = homepageUrl;
    }

    public String getTrailerUrl() {
        return trailerUrl;
    }

    public void setTrailerUrl(String trailerUrl) {
        this.trailerUrl = trailerUrl;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public List<Cast> getCastList() {
        return castList;
    }

    public void setCastList(List<Cast> castList) {
        this.castList = castList;
    }

    public List<Backdrop> getBackdropList() {
        return backdropList;
    }

    public void setBackdropList(List<Backdrop> backdropList) {
        this.backdropList = backdropList;
    }

    public int getRuntimeMin() {
        return runtimeMin;
    }

    public void setRuntimeMin(int runtimeMin) {
        this.runtimeMin = runtimeMin;
    }

    public DataViewModel() {
        this.title = "fake";
    }

}
