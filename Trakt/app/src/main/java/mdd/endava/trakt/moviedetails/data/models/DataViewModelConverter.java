package mdd.endava.trakt.moviedetails.data.models;

import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.database.model.MovieDBModel;
import mdd.endava.trakt.database.model.MovieDetails;
import mdd.endava.trakt.database.model.TvshowDetails;
import mdd.endava.trakt.database.model.TVShowDBModel;
import mdd.endava.trakt.models.moviedetails.MovieDetailed;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.moviepeople.Cast;
import mdd.endava.trakt.models.tvdetails.TvDetailed;
import mdd.endava.trakt.moviedetails.MovieOps;

import static mdd.endava.trakt.moviedetails.data.models.DataViewModel.YOUTUBE_URL;
import static mdd.endava.trakt.moviedetails.data.models.MovieDataViewModel.IMDB_URL;

public class DataViewModelConverter {

    public DataViewModel fromMovie(MovieDBModel movie) {
        MovieDataViewModel detailsData = new MovieDataViewModel();

        detailsData.setTitle(movie.getTitle());
        detailsData.setYear(MovieOps.getYear(movie.getReleaseDate()));
        detailsData.setOverview(movie.getOverview());
        detailsData.setOriginalLanguage(movie.getOriginalLanguage());
        detailsData.setReleaseDate(movie.getReleaseDate());
        detailsData.setVoteAverage(movie.getVoteAverage());
        detailsData.setRuntime("-");
        List<Backdrop> list = new ArrayList<>();
        list.add(new Backdrop(movie.getBackdropImage()));
        detailsData.setBackdropList(list);

        return detailsData;
    }

    public DataViewModel fromTvshow(TVShowDBModel tvShow) {
        TvshowDataViewModel detailsData = new TvshowDataViewModel();

        detailsData.setTitle(tvShow.getTitle());
        detailsData.setYear(MovieOps.getYear(tvShow.getFirstAirDate()));
        detailsData.setOverview(tvShow.getOverview());
        detailsData.setOriginalLanguage(tvShow.getOriginalLanguage());
        detailsData.setFirstAirDate(tvShow.getFirstAirDate());
        detailsData.setVoteAverage(tvShow.getVoteAverage());
        detailsData.setRuntime("-");
        List<Backdrop> list = new ArrayList<>();
        list.add(new Backdrop(tvShow.getBackdropImage()));
        detailsData.setBackdropList(list);

        return detailsData;
    }

    public DataViewModel fromMovieAndDetails(MovieDBModel movie, MovieDetails movieDetails) {
        MovieDataViewModel data = (MovieDataViewModel) fromMovie(movie);

        data.setHomepageUrl(movieDetails.getHomepage());
        data.setYoutubeId(movieDetails.getYoutubeId());
        if (data.getYoutubeId() != null) {
            data.setTrailerUrl(YOUTUBE_URL + data.getYoutubeId());
        } else {
            data.setTrailerUrl(null);
        }
        data.setTrailerUrl(YOUTUBE_URL + data.getYoutubeId());
        data.setTagline(movieDetails.getTagline());
        data.setImdbId(movieDetails.getImdbId());
        data.setImdbUrl(IMDB_URL + data.getImdbId());
        data.setRuntimeMin(movieDetails.getRuntimeMin());
        data.setRuntime(MovieOps.getTimeInGoodFormat(data.getRuntimeMin()));
        data.setGenresList(movieDetails.getGenreList());
        data.setGenres(MovieOps.concatArray(data.getGenresList()));
        data.setProdCountriesList(movieDetails.getProdCountriesList());
        data.setProdCountries(MovieOps.concatArray(data.getProdCountriesList()));
        data.setBackdropList(movieDetails.getBackdropsList());
        data.setCastList(movieDetails.getCastList());

        return data;
    }

    public DataViewModel fromTvshowAndDetails(TVShowDBModel tvShow, TvshowDetails tvshowDetails) {
        TvshowDataViewModel tvshowData = (TvshowDataViewModel) fromTvshow(tvShow);

        tvshowData.setTrailerUrl(tvshowData.getTrailerUrl());
        tvshowData.setHomepageUrl(tvshowDetails.getHomepage());
        tvshowData.setRuntime(tvshowDetails.getRuntime());
        tvshowData.setGenres(tvshowDetails.getGenres());
        tvshowData.setCastList(tvshowDetails.getCastList());
        tvshowData.setBackdropList(tvshowDetails.getBackdropsList());
        tvshowData.setOriginCountries(tvshowDetails.getOriginCountries());
        tvshowData.setFirstAirDate(tvshowDetails.getFirstAirDate());
        tvshowData.setStatus(tvshowDetails.getStatus());
        tvshowData.setNetworks(tvshowDetails.getNetworks());
        tvshowData.setSeasons(tvshowDetails.getSeasons());

        return tvshowData;
    }

    public DataViewModel fromMovieRequests(MovieDetailed movieDetailed, String youtubeKey, List<Cast> castList, List<Backdrop> backdropList) {
        MovieDataViewModel detailsData = new MovieDataViewModel();

        if (movieDetailed != null) {
            detailsData.setTitle(movieDetailed.getTitle());
            detailsData.setYear(MovieOps.getYear(movieDetailed.getReleaseDate()));
            detailsData.setOverview(movieDetailed.getOverview());
            detailsData.setOriginalLanguage(movieDetailed.getOriginalLanguage());
            detailsData.setReleaseDate(movieDetailed.getReleaseDate());
            detailsData.setVoteAverage(movieDetailed.getVoteAverage());
            detailsData.setTagline(movieDetailed.getTagline());
            detailsData.setRuntimeMin(movieDetailed.getRuntime());
            detailsData.setRuntime(MovieOps.getTimeInGoodFormat(detailsData.getRuntimeMin()));
            detailsData.setImdbId(movieDetailed.getImdbId());
            detailsData.setImdbUrl(IMDB_URL + detailsData.getImdbId());
            detailsData.setHomepageUrl(movieDetailed.getHomepage());
            detailsData.setGenresList(movieDetailed.getGenres());
            detailsData.setGenres(MovieOps.concatArray(detailsData.getGenresList()));

            detailsData.setProdCountriesList(movieDetailed.getProductionCountries());
            detailsData.setProdCountries(MovieOps.concatArray(detailsData.getProdCountriesList()));
        }
        if (backdropList == null && movieDetailed != null) {
            List<Backdrop> list = new ArrayList<>();
            list.add(new Backdrop(movieDetailed.getBackdropPath()));
            detailsData.setBackdropList(list);
        } else if (backdropList != null) {
            detailsData.setBackdropList(backdropList);
        }
        detailsData.setYoutubeId(youtubeKey);
        if (youtubeKey != null) {
            detailsData.setTrailerUrl(YOUTUBE_URL + youtubeKey);
        } else {
            detailsData.setTrailerUrl(null);
        }
        detailsData.setCastList(castList);

        return detailsData;
    }

    public DataViewModel fromTvShowRequests(TvDetailed tvShowDetailed, String youtubeKey, List<Cast> castList, List<Backdrop> backdropList) {
        TvshowDataViewModel tvshowData = new TvshowDataViewModel();

        if (tvShowDetailed != null) {
            tvshowData.setStatus(tvShowDetailed.getStatus());
            tvshowData.setTitle(tvShowDetailed.getName());
            tvshowData.setYear(MovieOps.getYear(tvShowDetailed.getFirstAirDate()));
            tvshowData.setOverview(tvShowDetailed.getOverview());
            tvshowData.setOriginalLanguage(tvShowDetailed.getOriginalLanguage());
            tvshowData.setFirstAirDate(tvShowDetailed.getFirstAirDate());
            tvshowData.setVoteAverage(tvShowDetailed.getVoteAverage());
            //tvshowData.setTagline(tvShowDetailed.getTagline());
            if (!tvShowDetailed.getEpisodeRunTime().isEmpty()) {
                tvshowData.setRuntimeMin(tvShowDetailed.getEpisodeRunTime().get(0));
                tvshowData.setRuntime(MovieOps.getTimeInGoodFormat(tvshowData.getRuntimeMin()));
            }
            // tvshowData.setImdbId(tvShowDetailed.getImdbId());
            // tvshowData.setImdbUrl(IMDB_URL + detailsData.getImdbId());
            tvshowData.setHomepageUrl(tvShowDetailed.getHomepage());
            tvshowData.setGenresList(tvShowDetailed.getGenres());
            tvshowData.setGenres(MovieOps.concatArray(tvshowData.getGenresList()));

            tvshowData.setOriginCountriesList(tvShowDetailed.getOriginCountry());
            tvshowData.setOriginCountries(MovieOps.concatArrayOfStrings(tvshowData.getOriginCountriesList()));

            tvshowData.setNetworkList(tvShowDetailed.getNetworks());
            tvshowData.setNetworks(MovieOps.concatArray(tvshowData.getNetworkList()));

            tvshowData.setSeasons(tvShowDetailed.getSeasons());
        }

        if (backdropList == null && tvShowDetailed != null) {
            List<Backdrop> list = new ArrayList<>();
            list.add(new Backdrop(tvShowDetailed.getBackdropPath()));
            tvshowData.setBackdropList(list);
        } else if (backdropList != null) {
            tvshowData.setBackdropList(backdropList);
        }

        tvshowData.setYoutubeId(youtubeKey);
        if (youtubeKey != null) {
            tvshowData.setTrailerUrl(YOUTUBE_URL + youtubeKey);
        } else {
            tvshowData.setTrailerUrl(null);
        }

        tvshowData.setCastList(castList);


        //TODO implement
        return tvshowData;
    }
}
