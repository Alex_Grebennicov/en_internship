package mdd.endava.trakt.moviedetails.data.models;

import java.util.Collections;
import java.util.List;

import mdd.endava.trakt.models.moviedetails.ProductionCountry;

/**
 * Created by User on 05.11.2017.
 */

public class MovieDataViewModel extends DataViewModel {

    public static final String IMDB_URL = "http://www.imdb.com/title/";

    private String releaseDate;
    private String tagline;
    private List<ProductionCountry> prodCountriesList = Collections.emptyList();
    private String prodCountries;
    private String imdbId;
    private String imdbUrl;

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<ProductionCountry> getProdCountriesList() {
        return prodCountriesList;
    }

    public void setProdCountriesList(List<ProductionCountry> prodCountriesList) {
        this.prodCountriesList = prodCountriesList;
    }
    public String getProdCountries() {
        return prodCountries;
    }

    public void setProdCountries(String prodCountries) {
        this.prodCountries = prodCountries;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getImdbUrl() {
        return imdbUrl;
    }

    public void setImdbUrl(String imdbUrl) {
        this.imdbUrl = imdbUrl;
    }
}
