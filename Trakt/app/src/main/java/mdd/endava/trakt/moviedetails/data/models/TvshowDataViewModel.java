package mdd.endava.trakt.moviedetails.data.models;

import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.models.tvdetails.Network;
import mdd.endava.trakt.models.tvdetails.Season;

/**
 * Created by User on 05.11.2017.
 */

public class TvshowDataViewModel extends DataViewModel {

    private String firstAirDate;
    private List<String> originCountriesList = new ArrayList<>();
    private String originCountries;
    private List<Network> networkList = new ArrayList<>();
    private String networks;
    private String status;
    private List<Season> seasons = new ArrayList<>();

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public void setFirstAirDate(String firstAirDate) {
        this.firstAirDate = firstAirDate;
    }

    public List<String> getOriginCountriesList() {
        return originCountriesList;
    }

    public void setOriginCountriesList(List<String> originCountriesList) {
        this.originCountriesList = originCountriesList;
    }

    public String getOriginCountries() {
        return originCountries;
    }

    public void setOriginCountries(String originCountries) {
        this.originCountries = originCountries;
    }

    public List<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(List<Network> networkList) {
        this.networkList = networkList;
    }

    public String getNetworks() {
        return networks;
    }

    public void setNetworks(String networks) {
        this.networks = networks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Season> getSeasons() {
        return seasons;
    }

    public void setSeasons(List<Season> seasons) {
        this.seasons = seasons;
    }
}
