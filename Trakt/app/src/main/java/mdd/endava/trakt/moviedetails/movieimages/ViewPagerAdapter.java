package mdd.endava.trakt.moviedetails.movieimages;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mdd.endava.trakt.R;
import mdd.endava.trakt.models.movieimages.Backdrop;

public class ViewPagerAdapter extends PagerAdapter {
    public static final String BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500";

    private Context context;
    private List<Backdrop> backdropsList = Collections.emptyList();

    public ViewPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return backdropsList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    public void setData(List<Backdrop> backdropsList) {
        if (backdropsList != null) {
            this.backdropsList = new ArrayList<>(backdropsList);
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_slider_images, container, false);
        ImageView imageView = view.findViewById(R.id.movie_backdrop);

        Picasso.with(context).load(BASE_URL_IMAGE + backdropsList.get(position).getFilePath())
                .error(R.color.colorPrimaryLight)
                .into(imageView);

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;

        viewPager.removeView(view);
    }

    public void clear() {
        backdropsList.clear();
        notifyDataSetChanged();
    }
}
