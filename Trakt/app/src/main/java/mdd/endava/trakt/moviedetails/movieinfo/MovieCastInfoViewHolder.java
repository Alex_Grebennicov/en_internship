package mdd.endava.trakt.moviedetails.movieinfo;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import mdd.endava.trakt.R;
import mdd.endava.trakt.models.MovieInfo;
import mdd.endava.trakt.models.moviepeople.Cast;

import static mdd.endava.trakt.moviedetails.movieimages.ViewPagerAdapter.BASE_URL_IMAGE;


public class MovieCastInfoViewHolder extends MovieInfoViewHolder {
    private View itemView;
    private TextView nameActor;
    private TextView nameCharacter;
    private ImageView posterActor;

    public MovieCastInfoViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        nameActor = itemView.findViewById(R.id.name_actor);
        nameCharacter = itemView.findViewById(R.id.name_character);
        posterActor = itemView.findViewById(R.id.actor_poster);
    }

    @Override
    public void bindData(MovieInfo item) {
        Cast person = (Cast) item;
        nameActor.setText(person.getName());
        nameCharacter.setText(person.getCharacter());
        Picasso.with(itemView.getContext()).load(BASE_URL_IMAGE + person.getProfilePath())
                .error(R.drawable.actor_photo_placeholder)
                .fit()
                .centerCrop()
                .into(posterActor, new Callback() {
                    @Override
                    public void onSuccess() {
                        Bitmap source = ((BitmapDrawable) posterActor.getDrawable()).getBitmap();

                        RoundedBitmapDrawable drawable =
                                RoundedBitmapDrawableFactory.create(itemView.getContext().getResources(), source);
                        drawable.setCircular(true);
                        drawable.setCornerRadius(Math.max(source.getWidth() / 2.0f, source.getHeight() / 2.0f));
                        posterActor.setImageDrawable(drawable);
                    }

                    @Override
                    public void onError() {

                    }
                });
    }


}
