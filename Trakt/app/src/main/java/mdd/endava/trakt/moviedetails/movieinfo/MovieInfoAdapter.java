package mdd.endava.trakt.moviedetails.movieinfo;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.R;
import mdd.endava.trakt.models.MovieInfo;

import static mdd.endava.trakt.models.MovieInfo.CAST_INFO_TYPE;
import static mdd.endava.trakt.models.MovieInfo.DETAILS_INFO_TYPE;

public class MovieInfoAdapter extends RecyclerView.Adapter<MovieInfoViewHolder> {
    private List<MovieInfo> infoList = new ArrayList<>();

    @Override
    public int getItemViewType(int position) {
        return infoList.get(position).getType();
    }

    @Override
    public MovieInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CAST_INFO_TYPE) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_casting_recyclerview, parent, false);
            return new MovieCastInfoViewHolder(view);
        } else if (viewType == DETAILS_INFO_TYPE) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_row_info, parent, false);
            return new MovieTextInfoViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(MovieInfoViewHolder holder, int position) {
        if (getItemViewType(position) == CAST_INFO_TYPE) {
            holder.bindData(infoList.get(position));
        } else if (getItemViewType(position) == DETAILS_INFO_TYPE) {
            holder.bindData(infoList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }

    public void setData(List<? extends MovieInfo> infoList) {
        this.infoList = (List<MovieInfo>) infoList;
        notifyDataSetChanged();
    }

    public void clear() {
        infoList.clear();
        notifyDataSetChanged();
    }
}
