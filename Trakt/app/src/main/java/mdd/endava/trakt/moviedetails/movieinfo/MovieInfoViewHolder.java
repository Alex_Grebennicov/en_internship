package mdd.endava.trakt.moviedetails.movieinfo;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import mdd.endava.trakt.models.MovieInfo;

public abstract class MovieInfoViewHolder extends RecyclerView.ViewHolder {

    public MovieInfoViewHolder(View itemView) {
        super(itemView);
    }

    public abstract void bindData(MovieInfo item);
}
