package mdd.endava.trakt.moviedetails.movieinfo;

import android.view.View;
import android.widget.TextView;

import mdd.endava.trakt.R;
import mdd.endava.trakt.models.MovieInfo;
import mdd.endava.trakt.models.MovieFeaturesInfo;

/**
 * Created by cmardari on 20-Oct-17.
 */

public class MovieTextInfoViewHolder extends MovieInfoViewHolder {
    private TextView title;
    private TextView content;

    public MovieTextInfoViewHolder(View itemView) {
        super(itemView);

        title = itemView.findViewById(R.id.title_info);
        content = itemView.findViewById(R.id.content_info);
    }

    @Override
    public void bindData(MovieInfo item) {
        MovieFeaturesInfo rowInfo = (MovieFeaturesInfo) item;

        title.setText(rowInfo.getCellTitle());
        content.setText(rowInfo.getCellContent());
    }
}
