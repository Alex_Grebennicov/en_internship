package mdd.endava.trakt.moviedetails.seasons;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import mdd.endava.trakt.R;
import mdd.endava.trakt.models.tvdetails.Season;
import mdd.endava.trakt.moviedetails.MovieOps;

import static mdd.endava.trakt.moviedetails.movieimages.ViewPagerAdapter.BASE_URL_IMAGE;

class SeasonViewHolder extends RecyclerView.ViewHolder {

    private TextView seasonNumberTextView;
    private TextView airdateTextView;
    private TextView numberOfEpisodesTextView;
    private ImageView seasonPoster;

    public SeasonViewHolder(View itemView) {
        super(itemView);
        seasonNumberTextView = itemView.findViewById(R.id.season_number);
        airdateTextView = itemView.findViewById(R.id.season_airdate);
        numberOfEpisodesTextView = itemView.findViewById(R.id.season_episodes_count);
        seasonPoster = itemView.findViewById(R.id.season_poster);
    }

    public void bindData(Season season) {
        seasonNumberTextView.setText(itemView.getContext().getString(R.string.season) + " " + season.getSeasonNumber());
        airdateTextView.setText(itemView.getContext().getString(R.string.air_date_title) + " " + MovieOps.getYear(season.getAirDate()));
        numberOfEpisodesTextView.setText(itemView.getContext().getString(R.string.episodes_title) + " " + String.valueOf(season.getEpisodeCount()));
        Picasso.with(itemView.getContext()).load(BASE_URL_IMAGE + season.getPosterPath())
                .error(R.color.colorPrimaryLight)
                .into(seasonPoster);
    }
}
