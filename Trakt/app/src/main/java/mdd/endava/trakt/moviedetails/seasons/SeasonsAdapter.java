package mdd.endava.trakt.moviedetails.seasons;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.R;
import mdd.endava.trakt.models.tvdetails.Season;

public class SeasonsAdapter extends RecyclerView.Adapter<SeasonViewHolder> {

    private List<Season> seasons = new ArrayList<>();

    @Override
    public SeasonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_season, parent, false);
        return new SeasonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SeasonViewHolder holder, int position) {
        holder.bindData(seasons.get(position));
    }

    @Override
    public int getItemCount() {
        return seasons.size();
    }

    public void setData(List<Season> seasons) {
        if (seasons != null) {
            this.seasons = new ArrayList<>(seasons);
            notifyDataSetChanged();
        }
    }

    public void clear() {
        seasons.clear();
        notifyDataSetChanged();
    }
}
