package mdd.endava.trakt.network;

import mdd.endava.trakt.models.moviedetails.MovieDetailed;
import mdd.endava.trakt.models.movieimages.Images;
import mdd.endava.trakt.models.moviepeople.Credits;
import mdd.endava.trakt.models.tvdetails.TvDetailed;
import mdd.endava.trakt.models.video.Video;
import mdd.endava.trakt.network.models.ParsedSignInResponse;
import mdd.endava.trakt.network.models.movies.ParsedMovieList;
import mdd.endava.trakt.network.models.tvshows.ParsedTVShowList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieDBAPI {
    int ITEMS_PER_PAGE = 20;
    String THUMB_IMAGE_LINK_PREFIX = "https://image.tmdb.org/t/p/w300";
    String API_KEY = "01c17b19a50ed1e7714d7bf9216048ff";

    String MOVIE_ID = "movie_id";
    String TV_ID = "tv_id";

    @GET("movie/{tag}?api_key=" + API_KEY)
    Call<ParsedMovieList> getMovieList(@Path("tag") String tag, @Query("page") String page);

    @GET("tv/{tag}?api_key=" + API_KEY)
    Call<ParsedTVShowList> getTVShowList(@Path("tag") String tag, @Query("page") String page);


    @GET("authentication/token/new?api_key=" + MovieDBAPI.API_KEY)
    Call<ParsedSignInResponse> getNewRequestToken();

    @GET("authentication/token/validate_with_login?api_key=" + MovieDBAPI.API_KEY)
    Call<ParsedSignInResponse> validateWithLogin(@Query("username") String username, @Query("password") String password, @Query("request_token") String requestToken);

    @GET("authentication/session/new?api_key=" + MovieDBAPI.API_KEY)
    Call<ParsedSignInResponse> getSessionID(@Query("request_token") String requestToken);


    @GET("movie/{" + MOVIE_ID + "}")
    Call<MovieDetailed> getMovieDetails(@Path(MOVIE_ID) String idMovie, @Query("api_key") String apiKey);

    @GET("movie/{" + MOVIE_ID + "}/images")
    Call<Images> getMovieImages(@Path(MOVIE_ID) String idMovie, @Query("api_key") String apiKey);

    @GET("movie/{" + MOVIE_ID + "}/credits")
    Call<Credits> getMovieCredits(@Path(MOVIE_ID) String idMovie, @Query("api_key") String apiKey);

    @GET("movie/{" + MOVIE_ID + "}/videos")
    Call<Video> getMovieVideos(@Path(MOVIE_ID) String idMovie, @Query("api_key") String apiKey);


    @GET("tv/{" + TV_ID + "}")
    Call<TvDetailed> getTvShowDetails(@Path(TV_ID) String idTv, @Query("api_key") String apiKey);

    @GET("tv/{" + TV_ID + "}/images")
    Call<Images> getTvShowImages(@Path(TV_ID) String idTv, @Query("api_key") String apiKey);

    @GET("tv/{" + TV_ID + "}/credits")
    Call<Credits> getTvShowCredits(@Path(TV_ID) String idTv, @Query("api_key") String apiKey);

    @GET("tv/{" + TV_ID + "}/videos")
    Call<Video> getTvShowVideo(@Path(TV_ID) String idTv, @Query("api_key") String apiKey);
}
