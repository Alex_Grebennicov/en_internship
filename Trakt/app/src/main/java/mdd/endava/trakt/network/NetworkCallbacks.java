package mdd.endava.trakt.network;

import mdd.endava.trakt.common.enums.AppError;

public interface NetworkCallbacks {
    void onErrorCaught(AppError error);

    void onSucceed();
}
