package mdd.endava.trakt.network.login;

public interface LoginNetwork {
    void trySignIn(String username, String password, LoginNetworkCallback callbacks);
}
