package mdd.endava.trakt.network.login;

import mdd.endava.trakt.common.enums.AppError;

public interface LoginNetworkCallback {
    void onErrorCaught(AppError error);

    void onLoginSucceed();
}
