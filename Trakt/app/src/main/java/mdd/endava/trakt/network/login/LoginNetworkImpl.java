package mdd.endava.trakt.network.login;

import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.network.login.request.LoginSessionGetRequest;
import mdd.endava.trakt.network.login.request.LoginTokenGenerateRequest;
import mdd.endava.trakt.network.login.request.LoginTokenValidateRequest;
import mdd.endava.trakt.network.NetworkCallbacks;
import mdd.endava.trakt.network.MovieDBAPI;

public class LoginNetworkImpl implements LoginNetwork {

    private final MovieDBAPI api;
    private final SecuredPreferences preferences;

    public LoginNetworkImpl(MovieDBAPI api, SecuredPreferences preferences) {
        this.preferences = preferences;
        this.api = api;
    }

    @Override
    public void trySignIn(final String username, final String password, final LoginNetworkCallback callbacks) {
        LoginTokenGenerateRequest request = new LoginTokenGenerateRequest(api, preferences, new NetworkCallbacks() {
            @Override
            public void onErrorCaught(AppError error) {
                callbacks.onErrorCaught(error);
            }

            @Override
            public void onSucceed() {
                validateTokenWithUsername(username, password, callbacks);
            }
        });
        request.getCall().enqueue(request);
    }

    private void validateTokenWithUsername(String username, String password, final LoginNetworkCallback callbacks) {
        LoginTokenValidateRequest request = new LoginTokenValidateRequest(api, username, password, preferences, new NetworkCallbacks() {
            @Override
            public void onErrorCaught(AppError error) {
                callbacks.onErrorCaught(error);
            }

            @Override
            public void onSucceed() {
                getSessionID(callbacks);
            }
        });
        request.getCall().enqueue(request);
    }

    private void getSessionID(final LoginNetworkCallback callbacks) {
        LoginSessionGetRequest request = new LoginSessionGetRequest(api, preferences, new NetworkCallbacks() {
            @Override
            public void onErrorCaught(AppError error) {
                callbacks.onErrorCaught(error);
            }

            @Override
            public void onSucceed() {
                callbacks.onLoginSucceed();
            }
        });
        request.getCall().enqueue(request);
    }
}
