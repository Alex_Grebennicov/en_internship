package mdd.endava.trakt.network.login.request;

import mdd.endava.trakt.network.NetworkCallbacks;
import mdd.endava.trakt.network.MovieDBAPI;
import retrofit2.Call;
import retrofit2.Callback;

public abstract class AbstractLoginRequest<RetrofitParsedObject>
        implements Callback<RetrofitParsedObject> {
    private final MovieDBAPI api;
    private final NetworkCallbacks callback;

    public AbstractLoginRequest(MovieDBAPI api, NetworkCallbacks callback) {
        this.api = api;
        this.callback = callback;
    }

    public abstract Call<RetrofitParsedObject> getCall();

    public MovieDBAPI getApi() {
        return api;
    }

    public NetworkCallbacks getCallback() {
        return callback;
    }
}
