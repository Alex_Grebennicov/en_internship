package mdd.endava.trakt.network.login.request;

import android.support.annotation.NonNull;

import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.network.NetworkCallbacks;
import mdd.endava.trakt.network.MovieDBAPI;
import mdd.endava.trakt.network.models.ParsedSignInResponse;
import retrofit2.Call;
import retrofit2.Response;

public class LoginTokenGenerateRequest extends AbstractLoginRequest<ParsedSignInResponse> {

    private final SecuredPreferences preferences;

    public LoginTokenGenerateRequest(MovieDBAPI api, SecuredPreferences preferences, NetworkCallbacks callback) {
        super(api, callback);
        this.preferences = preferences;
    }

    @Override
    public Call<ParsedSignInResponse> getCall() {
        return getApi().getNewRequestToken();
    }

    @Override
    public void onResponse(@NonNull Call<ParsedSignInResponse> call, @NonNull Response<ParsedSignInResponse> response) {
        if (response.isSuccessful() && response.body() != null) {
            preferences.setRequestToken(response.body().getRequestToken());
            getCallback().onSucceed();
        } else {
            getCallback().onErrorCaught(AppError.SERVER_ERROR);
        }
    }

    @Override
    public void onFailure(@NonNull Call<ParsedSignInResponse> call, @NonNull Throwable t) {
        getCallback().onErrorCaught(AppError.SYSTEM_ERROR);
    }
}
