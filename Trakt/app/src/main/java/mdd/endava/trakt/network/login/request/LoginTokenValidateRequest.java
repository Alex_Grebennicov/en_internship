package mdd.endava.trakt.network.login.request;

import android.support.annotation.NonNull;

import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.network.NetworkCallbacks;
import mdd.endava.trakt.network.MovieDBAPI;
import mdd.endava.trakt.network.models.ParsedSignInResponse;
import retrofit2.Call;
import retrofit2.Response;

public class LoginTokenValidateRequest extends AbstractLoginRequest<ParsedSignInResponse> {

    private final String username;
    private final String password;
    private final SecuredPreferences preferences;

    public LoginTokenValidateRequest(MovieDBAPI api, String username, String password, SecuredPreferences preferences, NetworkCallbacks callback) {
        super(api, callback);
        this.username = username;
        this.password = password;
        this.preferences = preferences;
    }

    @Override
    public Call<ParsedSignInResponse> getCall() {
        return getApi().validateWithLogin(username, password, preferences.getRequestToken());
    }

    @Override
    public void onResponse(@NonNull Call<ParsedSignInResponse> call, @NonNull Response<ParsedSignInResponse> response) {
        if (response.isSuccessful()) {
            getCallback().onSucceed();
        } else if (response.code() == 401) {
            getCallback().onErrorCaught(AppError.INVALID_CREDENTIALS);
        } else {
            getCallback().onErrorCaught(AppError.SERVER_ERROR);
        }
    }

    @Override
    public void onFailure(@NonNull Call<ParsedSignInResponse> call, @NonNull Throwable t) {
        getCallback().onErrorCaught(AppError.SYSTEM_ERROR);
    }
}
