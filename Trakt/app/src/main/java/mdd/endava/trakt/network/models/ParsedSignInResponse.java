package mdd.endava.trakt.network.models;

import com.google.gson.annotations.SerializedName;

public class ParsedSignInResponse {
    private boolean success;
    @SerializedName("request_token")
    private String requestToken;
    @SerializedName("session_id")
    private String sessionID;

    public boolean isSuccess() {
        return success;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public String getSessionID() {
        return sessionID;
    }
}
