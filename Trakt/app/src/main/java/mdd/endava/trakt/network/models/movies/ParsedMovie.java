package mdd.endava.trakt.network.models.movies;

import com.google.gson.annotations.SerializedName;

public class ParsedMovie {
    private long id;
    private String overview;
    private String title;
    private boolean adult;
    private float popularity;
    @SerializedName("release_date")
    private String releaseDate;
    @SerializedName("vote_average")
    private float voteAverage;
    @SerializedName("poster_path")
    private String thumbPathSuffix;
    @SerializedName("backdrop_path")
    private String backdropImagePathSuffix;
    @SerializedName("original_language")
    private String originalLanguage;

    public String getBackdropImagePathSuffix() {
        return backdropImagePathSuffix;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public long getId() {
        return id;
    }

    public String getOverview() {
        return overview;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAdult() {
        return adult;
    }

    public float getPopularity() {
        return popularity;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public String getThumbPathSuffix() {
        return thumbPathSuffix;
    }
}
