package mdd.endava.trakt.network.models.movies;

import java.util.List;

public class ParsedMovieList {
    private List<ParsedMovie> results;

    public List<ParsedMovie> getResults() {
        return results;
    }
}