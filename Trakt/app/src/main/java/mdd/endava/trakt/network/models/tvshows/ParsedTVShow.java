package mdd.endava.trakt.network.models.tvshows;

import com.google.gson.annotations.SerializedName;

public class ParsedTVShow {
    private long id;
    private String overview;
    private float popularity;
    @SerializedName("name")
    private String title;
    @SerializedName("first_air_date")
    private String firstAirDate;
    @SerializedName("vote_count")
    private int voteCount;
    @SerializedName("vote_average")
    private float voteAverage;
    @SerializedName("poster_path")
    private String thumbPathSuffix;
    @SerializedName("backdrop_path")
    private String backdropImagePathSuffix;
    @SerializedName("original_language")
    private String originalLanguage;

    public long getId() {
        return id;
    }

    public String getOverview() {
        return overview;
    }

    public float getPopularity() {
        return popularity;
    }

    public String getTitle() {
        return title;
    }

    public String getFirstAirDate() {
        return firstAirDate;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public float getVoteAverage() {
        return voteAverage;
    }

    public String getThumbPathSuffix() {
        return thumbPathSuffix;
    }

    public String getBackdropImagePathSuffix() {
        return backdropImagePathSuffix;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }
}
