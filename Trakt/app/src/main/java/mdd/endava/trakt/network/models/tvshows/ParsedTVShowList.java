package mdd.endava.trakt.network.models.tvshows;

import java.util.List;

public class ParsedTVShowList {
    private List<ParsedTVShow> results;

    public List<ParsedTVShow> getResults() {
        return results;
    }
}
