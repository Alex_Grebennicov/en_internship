package mdd.endava.trakt.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import mdd.endava.trakt.R;
import mdd.endava.trakt.login.LoginActivity;

public class SplashActivity extends AppCompatActivity implements Runnable {
    final int UPDATE_PROGRESSBAR_INTERVAL_MS = 20;
    final int MAX_PROGRESSBAR = 100;

    ProgressBar progressBar;
    TextView textView;
    Handler handler = new Handler();
    int iterator = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = findViewById(R.id.progress_bar);
        textView = findViewById(R.id.textview);
        textView.setText(getString(R.string.splash_screen_loading_label));

        new Thread(SplashActivity.this).start();
    }

    @Override
    public void run() {
        while (iterator++ < MAX_PROGRESSBAR) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    progressBar.incrementProgressBy(1);
                }
            });
            SystemClock.sleep(UPDATE_PROGRESSBAR_INTERVAL_MS);
        }
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
