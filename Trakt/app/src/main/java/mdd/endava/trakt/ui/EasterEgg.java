package mdd.endava.trakt.ui;

public class EasterEgg {
    public static String getRandomSmile() {
        char c = '\ude00';
        c += (int) (Math.random() * 30);
        return "\ud83d" + Character.toString(c);
    }
}
