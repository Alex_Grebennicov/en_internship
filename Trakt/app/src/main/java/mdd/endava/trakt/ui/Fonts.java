package mdd.endava.trakt.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.TextView;

public class Fonts {
    private static final String ARIAL_FONT_PATH = "asserts/arial.ttf";
    private static Typeface arialTypeface = null;

    public static void setArialFont(Context context, View... views) {
        for (View v : views) {
            if (arialTypeface == null) {
                arialTypeface = Typeface.createFromAsset(context.getAssets(), ARIAL_FONT_PATH);
            }
            applyTypeface(v, arialTypeface);
        }
    }

    private static void applyTypeface(View view, Typeface typeface) {
        if (view instanceof TextView) {
            ((TextView) view).setTypeface(typeface);
        } else if (view instanceof TextInputLayout) {
            ((TextInputLayout) view).setTypeface(typeface);
        }
    }
}
