package mdd.endava.trakt;

import org.junit.Test;

import mdd.endava.trakt.moviedetails.MovieOps;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MovieOpsTest {
    @Test
    public void getTimeInGoodFormatTest() throws Exception {
        assertEquals(MovieOps.getTimeInGoodFormat(152), "2h 32m");
        assertEquals(MovieOps.getTimeInGoodFormat(52), "52m");
        assertEquals(MovieOps.getTimeInGoodFormat(60), "1h");
        assertEquals(MovieOps.getTimeInGoodFormat(1440), "24h");
        assertEquals(MovieOps.getTimeInGoodFormat(61), "1h 1m");
        assertEquals(MovieOps.getTimeInGoodFormat(0), "-");
        assertEquals(MovieOps.getTimeInGoodFormat(-10), "-");
    }
}