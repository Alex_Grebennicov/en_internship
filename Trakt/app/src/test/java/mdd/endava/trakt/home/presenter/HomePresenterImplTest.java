package mdd.endava.trakt.home.presenter;


import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.common.IModel;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.home.repository.HomeMovieRepository;
import mdd.endava.trakt.home.view.HomeView;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.MovieDBAPI;

public class HomePresenterImplTest {

    private final static int PAGE_NUMBER_1 = 1;
    private final static int PAGE_NUMBER_2 = 2;
    private final static int ADAPTER_SIZE_20 = 20;
    private final static int INDEX_ZERO = 0;

    private HomePresenterImpl presenter;
    private List<IModel> emptyList;
    private AppError anyError;

    @Mock
    private HomeView view;
    @Mock
    private HomeMovieRepository repository;
    @Mock
    private Device device;
    @Mock
    private List<IModel> mockedPopulatedList;

    @Before
    public void setUp() {
        emptyList = new ArrayList<>();
        anyError = AppError.values()[INDEX_ZERO];

        MockitoAnnotations.initMocks(this);
        presenter = new HomePresenterImpl(repository, device);

        Mockito.when(mockedPopulatedList.size()).thenReturn(MovieDBAPI.ITEMS_PER_PAGE);
    }

    @Test
    public void onViewRefresh_shouldRequestDataFromFirstPage() {
        presenter.onViewRefresh();

        Mockito.verify(repository).onRequestData(device.isOnline(), HomePresenter.PAGE_NUMBER_1, presenter);
    }

    @Test
    public void onViewScrollEnd_viewNullShouldDoNothing() {
        presenter.onViewStop();

        Mockito.verifyZeroInteractions(view, repository);
    }

    @Test
    public void onViewScrollEnd_viewNotNullShouldShowProgressAndRequestData() {
        presenter.onViewStart(view);
        resetMocks();
        presenter.onViewScrollEnd(ADAPTER_SIZE_20);

        Mockito.verify(view).showProgress();
        Mockito.verify(repository).onRequestData(device.isOnline(), PAGE_NUMBER_2, presenter);
    }

    @Test
    public void onErrorCaught_viewNullShouldDoNothing() {
        presenter.onViewStop();
        presenter.onErrorCaught(anyError);

        Mockito.verifyZeroInteractions(view);
    }

    @Test
    public void onErrorCaught_viewNotNullShouldShowError() {
        presenter.onViewStart(view);
        Mockito.reset(view, repository, device);
        presenter.onErrorCaught(anyError);

        Mockito.verify(view).showError(anyError);
    }

    @Test
    public void onListReceived_viewNull_shouldDoNothing() {
        presenter.onViewStop();
        presenter.onListReceived(PAGE_NUMBER_1, mockedPopulatedList);

        Mockito.verifyZeroInteractions(view);
    }

    @Test
    public void onListReceived_viewNotNullAndFirstPageAndEmptyListAndDeviceOnline_shouldAddToDataList() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(true);
        presenter.onListReceived(PAGE_NUMBER_1, emptyList);

        Mockito.verify(view).addToDataList(emptyList);
    }

    @Test
    public void onListReceived_viewNotNullAndFirstPageAndEmptyListAndDeviceOffline_shouldShowNoNetworkError() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(false);
        presenter.onListReceived(PAGE_NUMBER_1, emptyList);

        Mockito.verify(view).showError(AppError.NO_NETWORK);
    }

    @Test
    public void onListReceived_viewNotNullAndFirstPageAndPopulatedListAndDeviceOnline_shouldRefreshDataList() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(true);
        presenter.onListReceived(PAGE_NUMBER_1, mockedPopulatedList);

        Mockito.verify(view).refreshDataList(mockedPopulatedList);
    }

    @Test
    public void onListReceived_viewNotNullAndFirstPageAndPopulatedListAndDeviceOffline_shouldRefreshDataList() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(false);
        presenter.onListReceived(PAGE_NUMBER_1, mockedPopulatedList);

        Mockito.verify(view).refreshDataList(mockedPopulatedList);
    }

    @Test
    public void onListReceived_viewNotNullAndNextPageAndEmptyListAndDeviceOnline_shouldAddToDataList() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(true);
        presenter.onListReceived(PAGE_NUMBER_2, emptyList);

        Mockito.verify(view).addToDataList(emptyList);
    }

    @Test
    public void onListReceived_viewNotNullAndNextPageAndEmptyListAndDeviceOffline_shouldShowNoNetworkError() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(false);
        presenter.onListReceived(PAGE_NUMBER_2, emptyList);

        Mockito.verify(view).showError(AppError.NO_NETWORK);
    }

    @Test
    public void onListReceived_viewNotNullAndNextPageAndPopulatedListAndDeviceOnline_shouldAddToDataList() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(true);
        presenter.onListReceived(PAGE_NUMBER_2, mockedPopulatedList);

        Mockito.verify(view).addToDataList(mockedPopulatedList);
    }

    @Test
    public void onListReceived_viewNotNullAndNextPageAndPopulatedListAndDeviceOffline_shouldAddToDataList() {
        presenter.onViewStart(view);
        resetMocks();
        Mockito.when(device.isOnline()).thenReturn(false);
        presenter.onListReceived(PAGE_NUMBER_2, mockedPopulatedList);

        Mockito.verify(view).addToDataList(mockedPopulatedList);
    }

    private void resetMocks() {
        Mockito.reset(view, repository, device);
    }
}