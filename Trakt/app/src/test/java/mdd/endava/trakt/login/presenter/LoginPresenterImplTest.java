package mdd.endava.trakt.login.presenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import mdd.endava.trakt.SecuredPreferences;
import mdd.endava.trakt.common.enums.AppError;
import mdd.endava.trakt.login.LoginPresenterImpl;
import mdd.endava.trakt.login.LoginView;
import mdd.endava.trakt.network.Device;
import mdd.endava.trakt.network.login.LoginNetwork;

public class LoginPresenterImplTest {

    private LoginPresenterImpl presenter;

    @Mock
    private LoginView view;
    @Mock
    private LoginNetwork network;
    @Mock
    private Device device;
    @Mock
    private SecuredPreferences preferences;

    private final String SESSION_ID_VALUE = "any";
    private final String VALID_USERNAME_VALUE = "user";
    private final String VALID_PASSWORD_VALUE = "pass123";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(preferences.getUsername()).thenReturn(VALID_USERNAME_VALUE);
        Mockito.when(preferences.getPassword()).thenReturn(VALID_PASSWORD_VALUE);
        Mockito.when(preferences.getSessionId()).thenReturn(SESSION_ID_VALUE);

        presenter = new LoginPresenterImpl(device, preferences, network);
    }

    @Test
    public void onViewStart_ifSessionIDNotNullShouldStartNextActivity() {
        Mockito.when(preferences.rememberCredentials()).thenReturn(true);

        presenter.onViewStart(view);
        Mockito.verify(preferences).getSessionId();
        Mockito.verify(view).startNextActivity();
        Mockito.verifyNoMoreInteractions(preferences, view);
    }

    @Test
    public void onViewStart_ifSessionNullWithRememberCredentialsShouldAutoCompleteCredentials() {
        Mockito.when(preferences.getSessionId()).thenReturn(null);
        Mockito.when(preferences.rememberCredentials()).thenReturn(true);

        presenter.onViewStart(view);
        Mockito.verify(preferences).getSessionId();
        Mockito.verify(preferences).rememberCredentials();
        Mockito.verify(view).setPassword(preferences.getPassword());
        Mockito.verify(view).setUsername(preferences.getUsername());
    }

    @Test
    public void onViewStart_ifSessionNullWithoutRememberCredentialsShouldDoNothingButVerifyingPrefs() {
        Mockito.when(preferences.getSessionId()).thenReturn(null);
        Mockito.when(preferences.rememberCredentials()).thenReturn(false);

        presenter.onViewStart(view);
        Mockito.verify(preferences).getSessionId();
        Mockito.verify(preferences).rememberCredentials();
        Mockito.verifyNoMoreInteractions(view, preferences);
    }

    @Test
    public void onLoginButtonPressed_validCredentialsWithoutRememberingDeviceOnlineShouldClearPrefsAndTrySignIn() {
        Mockito.when(device.isOnline()).thenReturn(true);
        Mockito.when(preferences.rememberCredentials()).thenReturn(false);
        presenter.onLoginButtonPressed(VALID_USERNAME_VALUE, VALID_PASSWORD_VALUE);

        Mockito.verify(preferences).rememberCredentials();
        Mockito.verify(preferences).setUsername(null);
        Mockito.verify(preferences).setPassword(null);
        Mockito.verify(device).isOnline();
        Mockito.verify(network).trySignIn(VALID_USERNAME_VALUE, VALID_PASSWORD_VALUE, presenter);
    }

    @Test
    public void onLoginButtonPressed_validCredentialsWithRememberingDeviceOnlineShouldRememberCredentialsAndTrySignIn() {
        Mockito.when(device.isOnline()).thenReturn(true);
        Mockito.when(preferences.rememberCredentials()).thenReturn(true);
        presenter.onLoginButtonPressed(VALID_USERNAME_VALUE, VALID_PASSWORD_VALUE);

        Mockito.verify(preferences).rememberCredentials();
        Mockito.verify(preferences).setUsername(VALID_USERNAME_VALUE);
        Mockito.verify(preferences).setPassword(VALID_PASSWORD_VALUE);
        Mockito.verify(device).isOnline();
        Mockito.verify(network).trySignIn(VALID_USERNAME_VALUE, VALID_PASSWORD_VALUE, presenter);
    }

    @Test
    public void onLoginButtonPressed_validCredentialsDeviceOffline() {
        Mockito.when(device.isOnline()).thenReturn(false);
        Mockito.when(preferences.rememberCredentials()).thenReturn(true);
        presenter.onLoginButtonPressed(VALID_USERNAME_VALUE, VALID_PASSWORD_VALUE);

        Mockito.verify(preferences).rememberCredentials();
        Mockito.verify(preferences).setUsername(VALID_USERNAME_VALUE);
        Mockito.verify(preferences).setPassword(VALID_PASSWORD_VALUE);
        Mockito.verify(device).isOnline();
        Mockito.verifyZeroInteractions(network);
    }

    @Test
    public void onRememberCredentialsChecked_checkedTrueShouldChangePrefsRememberCredentials() {
        presenter.onRememberCredentialsChecked(true);
        Mockito.verify(preferences).setRememberCredentials(true);
    }

    @Test
    public void onRememberCredentialsChecked_checkedFalseShouldChangePrefsRememberCredentials() {
        presenter.onRememberCredentialsChecked(false);
        Mockito.verify(preferences).setRememberCredentials(false);
    }

    @Test
    public void onErrorCaught_ifViewNullShouldNotInteractWithView() {
        presenter.onErrorCaught(AppError.values()[0]);
        Mockito.verifyZeroInteractions(view);
    }

    @Test
    public void onErrorCaught_ifViewNotNullShouldInteractWithView() {
        presenter.onViewStart(view);
        Mockito.reset(view);
        presenter.onErrorCaught(AppError.values()[0]);
        Mockito.verify(view).showError(AppError.values()[0]);
    }

    @Test
    public void onLoginSucceed_ifViewNullShouldNotInteractWithView() {
        presenter.onLoginSucceed();
        Mockito.verifyZeroInteractions(view);
    }

    @Test
    public void onLoginSucceed_ifViewNotNullShouldStartNextActivity() {
        presenter.onViewStart(view);
        Mockito.reset(view);

        presenter.onLoginSucceed();
        Mockito.verify(view).startNextActivity();
    }
}