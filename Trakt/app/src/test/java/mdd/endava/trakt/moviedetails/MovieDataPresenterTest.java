package mdd.endava.trakt.moviedetails;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import mdd.endava.trakt.R;
import mdd.endava.trakt.common.enums.ContentType;
import mdd.endava.trakt.moviedetails.data.asyncupdate.LoaderData;
import mdd.endava.trakt.moviedetails.data.asyncupdate.UpdateDataCallback;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.network.Device;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieDataPresenterTest {
    private final long FAKE_ID = 19;
    private final DataViewModel FAKE_DATA = new DataViewModel();
    private final int NORESPONSE_ERROR = R.string.no_response;
    private final int NOINTERNET_ERROR = R.string.no_internet_connection;
    private final ContentType MOVIE_TYPE = ContentType.MOVIE;
    private final ContentType TV_TYPE = ContentType.TV_SHOW;
    private final ContentType INVALID_TYPE = null;

    private MovieDetailsPresenter movieDetailsPresenter;

    @Mock
    private MovieDetailsView mockMovieDetailsView;

    @Mock
    private MovieDetailsBusiness mockMovieDetailsBussines;

    @Mock
    private Device mockDevice;

    @Mock
    private LoaderData mockLoaderData;


    @Captor
    private ArgumentCaptor<UpdateDataCallback> updateDataCallback;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        movieDetailsPresenter = new MovieDetailsPresenter(mockMovieDetailsView, mockMovieDetailsBussines, mockDevice, mockLoaderData);
    }

    @Test
    public void loadMovieDetails_whenOffline_showIntoView() {
        when(mockDevice.isOnline()).thenReturn(false);

        movieDetailsPresenter.onStart(FAKE_ID, MOVIE_TYPE);
        DataViewModel movie = verify(mockMovieDetailsBussines).getMovieData(eq(FAKE_ID));
        verify(mockMovieDetailsView).showData(eq(movie));
        verify(mockMovieDetailsView).setProgressBar(false);
        verify(mockMovieDetailsView).showError(eq(NOINTERNET_ERROR));
    }

    @Test
    public void loadTvshowDetails_whenOffline_showIntoView() {
        when(mockDevice.isOnline()).thenReturn(false);

        movieDetailsPresenter.onStart(FAKE_ID, TV_TYPE);
        DataViewModel movie = verify(mockMovieDetailsBussines).getTvshowData(eq(FAKE_ID));
        verify(mockMovieDetailsView).showData(eq(movie));
        verify(mockMovieDetailsView).setProgressBar(false);
        verify(mockMovieDetailsView).showError(eq(NOINTERNET_ERROR));
    }

    @Test
    public void loadMovieDetails_whenOnline_showIntoView() throws Exception {
        when(mockDevice.isOnline()).thenReturn(true);
        movieDetailsPresenter.onStart(FAKE_ID, MOVIE_TYPE);

        DataViewModel movie = verify(mockMovieDetailsBussines).getMovieData(eq(FAKE_ID));
        verify(mockMovieDetailsView).showData(eq(movie));
        verify(mockMovieDetailsView).setProgressBar(true);
        verify(mockLoaderData).loadData(eq(FAKE_ID), eq(MOVIE_TYPE), eq(mockMovieDetailsBussines), updateDataCallback.capture());

        updateDataCallback.getValue().onSuccess(FAKE_DATA);
        verify(mockMovieDetailsView).setProgressBar(false);
        verify(mockMovieDetailsView).showData(eq(FAKE_DATA));

        updateDataCallback.getValue().onSaveData(FAKE_DATA);
        verify(mockMovieDetailsBussines).saveMovieData(eq(FAKE_ID), eq(FAKE_DATA));
    }

    @Test
    public void loadTvshowDetails_whenOnline_showIntoView() throws Exception {
        when(mockDevice.isOnline()).thenReturn(true);
        movieDetailsPresenter.onStart(FAKE_ID, TV_TYPE);

        DataViewModel tvshowData = verify(mockMovieDetailsBussines).getTvshowData(eq(FAKE_ID));
        verify(mockMovieDetailsView).showData(eq(tvshowData));
        verify(mockMovieDetailsView).setProgressBar(true);
        verify(mockLoaderData).loadData(eq(FAKE_ID), eq(TV_TYPE), eq(mockMovieDetailsBussines), updateDataCallback.capture());

        updateDataCallback.getValue().onSuccess(FAKE_DATA);
        verify(mockMovieDetailsView).setProgressBar(false);
        verify(mockMovieDetailsView).showData(eq(FAKE_DATA));

        updateDataCallback.getValue().onSaveData(FAKE_DATA);
        verify(mockMovieDetailsBussines).saveTvshowData(eq(FAKE_ID), eq(FAKE_DATA));
    }

    @Test
    public void failLoadingOnlineMovieDetails_showErrorIntoView() {
        when(mockDevice.isOnline()).thenReturn(true);
        movieDetailsPresenter.onStart(FAKE_ID, MOVIE_TYPE);

        DataViewModel movie = verify(mockMovieDetailsBussines).getMovieData(eq(FAKE_ID));
        verify(mockMovieDetailsView).showData(eq(movie));
        verify(mockMovieDetailsView).setProgressBar(true);
        verify(mockLoaderData).loadData(eq(FAKE_ID), eq(MOVIE_TYPE), eq(mockMovieDetailsBussines), updateDataCallback.capture());

        updateDataCallback.getValue().onFailure(NORESPONSE_ERROR);
        verify(mockMovieDetailsView).showError(eq(NORESPONSE_ERROR));
    }

    @Test
    public void failLoadingOnlineTvshowDetails_showErrorIntoView() {
        when(mockDevice.isOnline()).thenReturn(true);
        movieDetailsPresenter.onStart(FAKE_ID, TV_TYPE);

        DataViewModel movie = verify(mockMovieDetailsBussines).getTvshowData(eq(FAKE_ID));
        verify(mockMovieDetailsView).showData(eq(movie));
        verify(mockMovieDetailsView).setProgressBar(true);
        verify(mockLoaderData).loadData(eq(FAKE_ID), eq(TV_TYPE), eq(mockMovieDetailsBussines), updateDataCallback.capture());

        updateDataCallback.getValue().onFailure(NORESPONSE_ERROR);
        verify(mockMovieDetailsView).showError(eq(NORESPONSE_ERROR));
    }

    @Test
    public void onSuccess_whenViewNotNull_showDataIntoView() {
        movieDetailsPresenter.onSuccess(FAKE_DATA);
        verify(mockMovieDetailsView).setProgressBar(false);
        verify(mockMovieDetailsView).showData(eq(FAKE_DATA));
    }

    @Test
    public void onSuccess_whenViewNull_doNothing() {
        movieDetailsPresenter.onDestroy();
        movieDetailsPresenter.onSuccess(FAKE_DATA);
        verify(mockMovieDetailsView, never()).showData(eq(FAKE_DATA));
    }

    @Test
    public void onFail_whenViewNotNull_showErrorIntoView() {
        movieDetailsPresenter.onFailure(NORESPONSE_ERROR);
        verify(mockMovieDetailsView).showError(eq(NORESPONSE_ERROR));
    }

    @Test
    public void onFail_whenViewNull_doNothing() {
        movieDetailsPresenter.onDestroy();
        movieDetailsPresenter.onFailure(NORESPONSE_ERROR);
        verify(mockMovieDetailsView, never()).showError(eq(NORESPONSE_ERROR));
    }

    @Test
    public void onSaveData_whenBusinessNull_doNothing() {
        movieDetailsPresenter.onDestroy();
        movieDetailsPresenter.onSaveData(FAKE_DATA);
        verify(mockMovieDetailsBussines, never()).saveMovieData(anyLong(), eq(FAKE_DATA));
        verify(mockMovieDetailsBussines, never()).saveTvshowData(anyLong(), eq(FAKE_DATA));
    }

    @Test
    public void invalidType_whenOnline_doNothing() {
        when(mockDevice.isOnline()).thenReturn(true);

        movieDetailsPresenter.onStart(FAKE_ID, INVALID_TYPE);
        verify(mockMovieDetailsBussines, never()).getTvshowData(eq(FAKE_ID));
        verify(mockMovieDetailsBussines, never()).getMovieData(eq(FAKE_ID));
        verify(mockMovieDetailsView).setProgressBar(true);
        verify(mockLoaderData).loadData(eq(FAKE_ID), eq(INVALID_TYPE), eq(mockMovieDetailsBussines), updateDataCallback.capture());

        updateDataCallback.getValue().onFailure(NORESPONSE_ERROR);
        verify(mockMovieDetailsView).showError(eq(NORESPONSE_ERROR));
    }

    @Test
    public void invalidType_whenOffline_doNothing() {
        when(mockDevice.isOnline()).thenReturn(false);

        movieDetailsPresenter.onStart(FAKE_ID, INVALID_TYPE);
        verify(mockMovieDetailsBussines, never()).getTvshowData(eq(FAKE_ID));
        verify(mockMovieDetailsBussines, never()).getMovieData(eq(FAKE_ID));
        verify(mockMovieDetailsView).setProgressBar(false);
        verify(mockMovieDetailsView).showError(NOINTERNET_ERROR);

    }

    @Test
    public void onDestroyPresenter_closeDb() {
        movieDetailsPresenter.onDestroy();
        verify(mockMovieDetailsBussines).closeDb();
    }
}

