package mdd.endava.trakt.moviedetails;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import mdd.endava.trakt.moviedetails.data.LocalDataSource;
import mdd.endava.trakt.moviedetails.data.RemoteDataSource;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieDetailsBusinessTest {

    private final DataViewModel FAKE_DATA = new DataViewModel();
    private final long FAKE_ID = 19;

    private MovieDetailsBusiness movieDetailsBusiness;

    @Mock
    private LocalDataSource localDataSource;

    @Mock
    private RemoteDataSource remoteDataSource;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        movieDetailsBusiness = new MovieDetailsBusiness(localDataSource, remoteDataSource);
    }

    @Test
    public void getMovieData() throws Exception {
        when(localDataSource.getMovieData(FAKE_ID)).thenReturn(FAKE_DATA);

        DataViewModel dataReturn = movieDetailsBusiness.getMovieData(FAKE_ID);
        verify(localDataSource).getMovieData(eq(FAKE_ID));
        assertEquals(FAKE_DATA, dataReturn);
    }

    @Test
    public void getNewMovieData() throws Exception {
        when(remoteDataSource.getMovieData(FAKE_ID)).thenReturn(FAKE_DATA);

        DataViewModel dataReturn = movieDetailsBusiness.getNewMovieData(FAKE_ID);
        verify(remoteDataSource).getMovieData(eq(FAKE_ID));
        assertEquals(FAKE_DATA, dataReturn);
    }

    @Test
    public void saveMovieData() throws Exception {
        movieDetailsBusiness.saveMovieData(FAKE_ID, FAKE_DATA);
        verify(localDataSource).saveOrUpdateMovieData(eq(FAKE_ID), eq(FAKE_DATA));
    }
}