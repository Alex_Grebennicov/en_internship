package mdd.endava.trakt.moviedetails.data;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import mdd.endava.trakt.database.RealmManager;
import mdd.endava.trakt.database.model.MovieDBModel;
import mdd.endava.trakt.database.model.MovieDetails;
import mdd.endava.trakt.database.model.TVShowDBModel;
import mdd.endava.trakt.database.model.TvshowDetails;
import mdd.endava.trakt.database.newdao.MovieDao;
import mdd.endava.trakt.database.newdao.MovieDetailsDao;
import mdd.endava.trakt.database.newdao.TvshowDao;
import mdd.endava.trakt.database.newdao.TvshowDetailsDao;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.DataViewModelConverter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class LocalDataSourceTest {
    private final long FAKE_ID = 55555;
    private final DataViewModel FAKE_DATA_VIEW_MODEL = new DataViewModel();
    private LocalDataSource localDataSource;

    @Mock
    private RealmManager realmManager;
    @Mock
    private DataViewModelConverter converter;

    @Mock
    MovieDao movieDao;

    @Mock
    MovieDetailsDao movieDetailsDao;

    @Mock
    TvshowDao tvshowDao;

    @Mock
    TvshowDetailsDao tvshowDetailsDao;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        localDataSource = new LocalDataSource(realmManager, converter);

        when(realmManager.createMovieDao()).thenReturn(movieDao);
        when(realmManager.createMovieDetailsDao()).thenReturn(movieDetailsDao);
        when(realmManager.createTvshowDao()).thenReturn(tvshowDao);
        when(realmManager.createTvshowDetailsDao()).thenReturn(tvshowDetailsDao);
    }

    @Test
    public void getMovieWithDetails() throws Exception {
        MovieDBModel FAKE_MOVIE = new MovieDBModel();
        MovieDetails FAKE_DETAILS = new MovieDetails();

        when(movieDao.getById(FAKE_ID)).thenReturn(FAKE_MOVIE);
        when(movieDetailsDao.getById(FAKE_ID)).thenReturn(FAKE_DETAILS);
        when(converter.fromMovieAndDetails(FAKE_MOVIE, FAKE_DETAILS)).thenReturn(FAKE_DATA_VIEW_MODEL);

        DataViewModel dataViewModel = localDataSource.getMovieData(FAKE_ID);
        verify(movieDetailsDao).getById(eq(FAKE_ID));
        verify(movieDao).getById(eq(FAKE_ID));

        verify(converter).fromMovieAndDetails(eq(FAKE_MOVIE), eq(FAKE_DETAILS));
        assertEquals(FAKE_DATA_VIEW_MODEL, dataViewModel);
    }

    @Test
    public void getMovieWithoutDetails() throws Exception {

        MovieDBModel FAKE_MOVIE = new MovieDBModel();
        MovieDetails FAKE_DETAILS = null;

        when(movieDao.getById(FAKE_ID)).thenReturn(FAKE_MOVIE);
        when(movieDetailsDao.getById(FAKE_ID)).thenReturn(FAKE_DETAILS);
        when(converter.fromMovie(FAKE_MOVIE)).thenReturn(FAKE_DATA_VIEW_MODEL);

        DataViewModel dataViewModel = localDataSource.getMovieData(FAKE_ID);
        verify(movieDetailsDao).getById(eq(FAKE_ID));
        verify(movieDao).getById(eq(FAKE_ID));

        verify(converter).fromMovie(eq(FAKE_MOVIE));
        assertEquals(FAKE_DATA_VIEW_MODEL, dataViewModel);
    }

    @Test
    public void movieNotFound_returnNullData() {
        MovieDBModel FAKE_MOVIE = null;
        MovieDetails FAKE_DETAILS = null;

        when(movieDao.getById(FAKE_ID)).thenReturn(FAKE_MOVIE);
        when(movieDetailsDao.getById(FAKE_ID)).thenReturn(FAKE_DETAILS);

        DataViewModel dataViewModel = localDataSource.getMovieData(FAKE_ID);
        verify(movieDetailsDao).getById(eq(FAKE_ID));
        verify(movieDao).getById(eq(FAKE_ID));

        verifyZeroInteractions(converter);
        assertEquals(null, dataViewModel);
    }

    @Test
    public void getTvshowWithDetails() throws Exception {
        TVShowDBModel FAKE_TVSHOW = new TVShowDBModel();
        TvshowDetails FAKE_DETAILS = new TvshowDetails();

        when(tvshowDao.getById(FAKE_ID)).thenReturn(FAKE_TVSHOW);
        when(tvshowDetailsDao.getById(FAKE_ID)).thenReturn(FAKE_DETAILS);
        when(converter.fromTvshowAndDetails(FAKE_TVSHOW, FAKE_DETAILS)).thenReturn(FAKE_DATA_VIEW_MODEL);

        DataViewModel dataViewModel = localDataSource.getTvShowData(FAKE_ID);
        verify(tvshowDetailsDao).getById(eq(FAKE_ID));
        verify(tvshowDao).getById(eq(FAKE_ID));

        verify(converter).fromTvshowAndDetails(eq(FAKE_TVSHOW), eq(FAKE_DETAILS));
        assertEquals(FAKE_DATA_VIEW_MODEL, dataViewModel);
    }

    @Test
    public void getTvshowWithoutDetails() throws Exception {
        TVShowDBModel FAKE_TVSHOW = new TVShowDBModel();
        TvshowDetails FAKE_DETAILS = null;

        when(tvshowDao.getById(FAKE_ID)).thenReturn(FAKE_TVSHOW);
        when(tvshowDetailsDao.getById(FAKE_ID)).thenReturn(FAKE_DETAILS);
        when(converter.fromTvshow(FAKE_TVSHOW)).thenReturn(FAKE_DATA_VIEW_MODEL);

        DataViewModel dataViewModel = localDataSource.getTvShowData(FAKE_ID);
        verify(tvshowDetailsDao).getById(eq(FAKE_ID));
        verify(tvshowDao).getById(eq(FAKE_ID));

        verify(converter).fromTvshow(eq(FAKE_TVSHOW));
        assertEquals(FAKE_DATA_VIEW_MODEL, dataViewModel);
    }

    @Test
    public void tvshowNotFound_returnNullData() {
        TVShowDBModel FAKE_TVSHOW = null;
        TvshowDetails FAKE_DETAILS = null;

        when(tvshowDao.getById(FAKE_ID)).thenReturn(FAKE_TVSHOW);
        when(tvshowDetailsDao.getById(FAKE_ID)).thenReturn(FAKE_DETAILS);

        DataViewModel dataViewModel = localDataSource.getTvShowData(FAKE_ID);
        verify(tvshowDetailsDao).getById(eq(FAKE_ID));
        verify(tvshowDao).getById(eq(FAKE_ID));
        verifyZeroInteractions(converter);

        assertEquals(null, dataViewModel);
    }

    @Test
    public void saveOrUpdateMovieData() {
        localDataSource.saveOrUpdateMovieData(FAKE_ID, FAKE_DATA_VIEW_MODEL);
        verify(movieDetailsDao).save(eq(FAKE_ID), eq(FAKE_DATA_VIEW_MODEL));

    }

    @Test
    public void saveOrUpdateTvshowData() {
        localDataSource.saveOrUpdateTvshowData(FAKE_ID, FAKE_DATA_VIEW_MODEL);
        verify(tvshowDetailsDao).save(eq(FAKE_ID), eq(FAKE_DATA_VIEW_MODEL));
    }

    @Test
    public void closeDb() {
        localDataSource.close();
        verify(realmManager).close();
    }

}