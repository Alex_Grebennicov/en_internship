package mdd.endava.trakt.moviedetails.data;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mdd.endava.trakt.models.moviedetails.MovieDetailed;
import mdd.endava.trakt.models.movieimages.Backdrop;
import mdd.endava.trakt.models.moviepeople.Cast;
import mdd.endava.trakt.models.tvdetails.TvDetailed;
import mdd.endava.trakt.moviedetails.data.models.DataViewModel;
import mdd.endava.trakt.moviedetails.data.models.DataViewModelConverter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RemoteDataSourceTest {

    private final DataViewModel FAKE_DATA = new DataViewModel();
    private final TvDetailed FAKE_TV_DETAILED = new TvDetailed();
    private final MovieDetailed FAKE_MOVIE_DETAILED = new MovieDetailed();
    private final String FAKE_YOUTUBE_KEY = "456156156";
    private final List<Cast> FAKE_CAST_LIST = new ArrayList<>();
    private final List<Backdrop> FAKE_BACKFROPS_LIST = new ArrayList<>();
    private final long FAKE_ID = 55555;

    private RemoteDataSource remoteDataSource;

    @Mock
    private MovieDetailsServiceApi movieDbApi;

    @Mock
    private DataViewModelConverter converter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        remoteDataSource = new RemoteDataSource(movieDbApi, converter);
    }

    @Test
    public void makeTvshowRequests_returnData() throws Exception {
        when(movieDbApi.getTvshowInfo(FAKE_ID)).thenReturn(FAKE_TV_DETAILED);
        when(movieDbApi.getTvshowYoutubeKey(FAKE_ID)).thenReturn(FAKE_YOUTUBE_KEY);
        when(movieDbApi.getTvshowCredits(FAKE_ID)).thenReturn(FAKE_CAST_LIST);
        when(movieDbApi.getTvshowImages(FAKE_ID)).thenReturn(FAKE_BACKFROPS_LIST);
        when(converter.fromTvShowRequests(FAKE_TV_DETAILED, FAKE_YOUTUBE_KEY, FAKE_CAST_LIST, FAKE_BACKFROPS_LIST)).thenReturn(FAKE_DATA);

        DataViewModel returnedData = remoteDataSource.getTvShowData(FAKE_ID);

        verify(movieDbApi).getTvshowInfo(eq(FAKE_ID));
        verify(movieDbApi).getTvshowYoutubeKey(eq(FAKE_ID));
        verify(movieDbApi).getTvshowCredits(eq(FAKE_ID));
        verify(movieDbApi).getTvshowImages(eq(FAKE_ID));
        verify(converter).fromTvShowRequests(eq(FAKE_TV_DETAILED), eq(FAKE_YOUTUBE_KEY), eq(FAKE_CAST_LIST), eq(FAKE_BACKFROPS_LIST));

        assertEquals(FAKE_DATA, returnedData);
    }

    @Test
    public void makeMovieRequests_returnData() throws Exception {
        when(movieDbApi.getMovieInfo(FAKE_ID)).thenReturn(FAKE_MOVIE_DETAILED);
        when(movieDbApi.getMovieYoutubeKey(FAKE_ID)).thenReturn(FAKE_YOUTUBE_KEY);
        when(movieDbApi.getMovieCredits(FAKE_ID)).thenReturn(FAKE_CAST_LIST);
        when(movieDbApi.getMovieImages(FAKE_ID)).thenReturn(FAKE_BACKFROPS_LIST);
        when(converter.fromMovieRequests(FAKE_MOVIE_DETAILED, FAKE_YOUTUBE_KEY, FAKE_CAST_LIST, FAKE_BACKFROPS_LIST)).thenReturn(FAKE_DATA);

        DataViewModel returnedData = remoteDataSource.getMovieData(FAKE_ID);

        verify(movieDbApi).getMovieInfo(eq(FAKE_ID));
        verify(movieDbApi).getMovieYoutubeKey(eq(FAKE_ID));
        verify(movieDbApi).getMovieCredits(eq(FAKE_ID));
        verify(movieDbApi).getMovieImages(eq(FAKE_ID));
        verify(converter).fromMovieRequests(eq(FAKE_MOVIE_DETAILED), eq(FAKE_YOUTUBE_KEY), eq(FAKE_CAST_LIST), eq(FAKE_BACKFROPS_LIST));

        assertEquals(FAKE_DATA, returnedData);
    }

    @Test
    public void makeMovieRequest_problemTalkingWithServer_returnNull() throws IOException {
        when(movieDbApi.getMovieInfo(FAKE_ID)).thenThrow(new IOException());

        DataViewModel returnedData = remoteDataSource.getMovieData(FAKE_ID);

        verify(converter, never()).fromMovieRequests(any(MovieDetailed.class), anyString(), anyListOf(Cast.class), anyListOf(Backdrop.class));

        assertEquals(null, returnedData);
    }

    @Test
    public void makeTvRequest_problemTalkingWithServer_returnNull() throws IOException {
        when(movieDbApi.getTvshowInfo(FAKE_ID)).thenThrow(new IOException());

        DataViewModel returnedData = remoteDataSource.getTvShowData(FAKE_ID);

        verify(converter, never()).fromTvShowRequests(any(TvDetailed.class), anyString(), anyListOf(Cast.class), anyListOf(Backdrop.class));

        assertEquals(null, returnedData);
    }
}